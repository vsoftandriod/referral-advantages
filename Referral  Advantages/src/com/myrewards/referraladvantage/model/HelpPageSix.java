package com.myrewards.referraladvantage.model;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.myrewards.referraladvantage.controller.R;
import com.myrewards.referraladvantage.utils.Utility;

public class HelpPageSix extends Fragment {
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v=(LinearLayout)inflater.inflate(R.layout.page6_frag6_layout, container, false);
		TextView textview1=(TextView)v.findViewById(R.id.page1_IVID);
		textview1.setMovementMethod(new ScrollingMovementMethod());
		textview1.setTypeface(Utility.font_reg);
		//textview1.setText(Html.fromHtml("<h4>  Your Coupon  </h4>&#9733; You now have the offer coupon on your <br> &nbsp&nbsp&nbsp&nbsp phone. How cool's that ! <br><br>&#9733;   Click the T&C button to see the terms and <br> &nbsp&nbsp&nbsp&nbsp conditions of use that apply to that <br> &nbsp&nbsp&nbsp&nbsp merchant.<br><br>&#9733; Read all offer terms and conditions <br> &nbsp&nbsp&nbsp&nbsp carefully before presentation to make sure <br> &nbsp&nbsp&nbsp&nbsp that you use the correct redemption option <br> &nbsp&nbsp&nbsp&nbsp as <b>some offers can only be redeemed <br> &nbsp&nbsp&nbsp&nbsp online</b>.<br><br>&#9733; Each coupon offer  has a <b> \"Merchant <br>&nbsp&nbsp&nbsp&nbsp Redeem \" </b> button. <b>THIS &nbsp&nbspIS FOR <br>&nbsp&nbsp&nbsp&nbsp MERCHANT USE ONLY </b>. Some offers <br>&nbsp&nbsp&nbsp&nbsp only allow a SINGLE USE, so don't waste <br>&nbsp&nbsp&nbsp&nbsp it by pressing the button. As once it's <br>&nbsp&nbsp&nbsp&nbsp gone... it's gone!<br><br> Happy saving... but if all else fails send an email to: <br> <a href=\"mailto:support@therewardsteam.com\" target=\"_blank\">support@therewardsteam.com</a>.   <br><br><br><br><br><br><br><br><br><br>"));
		textview1.setText(Html.fromHtml("<h4>  Your Coupon  </h4>&#9733; You now have the offer coupon on your phone. How cool's that ! <br><br>&#9733;   Click the T&C button to see the terms and conditions of use that apply to that merchant.<br><br>&#9733; Read all offer terms and conditions carefully before presentation to make sure that you use the correct redemption option as <b>some offers can only be redeemed online</b>.<br><br>&#9733; Each coupon offer  has a <b> \"Merchant Redeem \" </b> button. <b>THIS IS FOR MERCHANT USE ONLY </b>. Some offers only allow a SINGLE USE, so don't waste it by pressing the button. As once it's gone... it's gone!<br><br> Happy saving... but if all else fails send an email to: <br> <a href=\"mailto:support@therewardsteam.com\" target=\"_blank\">support@therewardsteam.com</a>.   <br><br><br><br><br><br><br><br><br><br>"));
		return v;
		
	}
}