package com.myrewards.referraladvantage.model;

public class ResultObject {
	private String productName;
	private String offer;
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getOffer() {
		return offer;
	}
	public void setOffer(String offer) {
		this.offer = offer;
	}
	
}
