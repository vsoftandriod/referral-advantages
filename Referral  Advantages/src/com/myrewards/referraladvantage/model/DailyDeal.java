package com.myrewards.referraladvantage.model;

public class DailyDeal {
	private String id;
	private String hotoffer_extension;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHotoffer_extension() {
		return hotoffer_extension;
	}
	public void setHotoffer_extension(String hotoffer_extension) {
		this.hotoffer_extension = hotoffer_extension;
	}
	
}
