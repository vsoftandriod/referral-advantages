package com.myrewards.referraladvantage.controller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.Utility;

public class MyMemberShipCardActivity extends HomeBasedActivity {

	boolean isClientbanner = true;
	SmartImageView cardIV;
	View loading;
	public static String headerTitle = null;
	TextView cardLoading;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_membership_card);
		registerBaseActivityReceiver();

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		headerTitle = getResources().getString(R.string.my_card_text);
		setHeaderTitle(headerTitle);
		
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuBtn.setOnClickListener(this);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		cardIV = (SmartImageView) findViewById(R.id.cardRLID);
		cardIV.getLayoutParams().width = 4 * (Utility.screenWidth / 6);
		cardIV.getLayoutParams().height = (int) ((1.5) * cardIV.getLayoutParams().width);
				
		cardLoading = (TextView)findViewById(R.id.cardLoadTVID);
		cardLoading.setTypeface(Utility.font_bold);

		try {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (Utility.user != null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				}
				if (Utility.user != null) {
					try {
						TextView clientNameTV = (TextView) findViewById(R.id.clientNameTVID);
						clientNameTV.setTypeface(Utility.font_bold);
						TextView nameTV = (TextView) findViewById(R.id.memberNameTVID);
						nameTV.setTypeface(Utility.font_bold);
						TextView membershipNumberTV = (TextView) findViewById(R.id.membershipNumberTVID);
						membershipNumberTV.setTypeface(Utility.font_bold);

						clientNameTV.setText(getResources().getString(R.string.client_name)+ " "+ Utility.user.getClient_name());
						nameTV.setText(getResources().getString(R.string.user_name)	+ " "+ Utility.user.getFirst_name() + " "+ Utility.user.getLast_name());
						membershipNumberTV.setText(getResources().getString(R.string.m_no_name)+ " "+ Utility.user.getUsername());
						
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("HARI-->DEBUG", e);
						}
					}
					if (Utility.user.getMyMembershipCard() == null) {
						String myMembershipCard = "MemberShipCard";
						String cardURL = ApplicationConstants.MEMBERSHIP_CARD_WRAPPER+ Utility.user.getClient_id()+ "."+ Utility.user.getCard_ext();
						if (Utility.user.getCard_ext() != null) {
							newCardImagesLoading(cardURL, myMembershipCard);
						} else{
							loading.setVisibility(View.GONE);
							cardLoading.setText("No Membership Card");
						}
					}
				}
			} else {
				// The Custom Toast Layout Imported here --> by HARI
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));

				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				MyMemberShipCardActivity.this.finish();
				headerTitle = null;
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}
	
	private void newCardImagesLoading(String cardURL, String myMembershipCard) {
		// Image url
		String image_url = cardURL;
		Log.w("Hari-->", cardURL);

		// ImageLoader class instance
		/*BannerImageLoader imgLoader = new BannerImageLoader(
				getApplicationContext(), myMembershipCard);*/

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView
		try {
			cardIV.setImageUrl(image_url);
		//	imgLoader.DisplayImage(image_url, cardIV);
			} catch (OutOfMemoryError e) {
				if ( e != null) {
					Log.w("Hari-->DEBUG", e);
			}
		}
		
		loading.setVisibility(View.GONE);
		cardLoading.setVisibility(View.GONE);
	}

	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;

		// int loader = R.drawable.loading_imp;
		// int loader = banner.getLayoutParams().width = Utility.screenWidth;

		// Image url
		String image_url = _bannerPath;
		Log.w("Hari-->", _bannerPath);

		// ImageLoader class instance
		/*BannerImageLoader imgLoader = new BannerImageLoader(
				getApplicationContext());*/

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView
		
		try {
			banner.setImageUrl(image_url);
		//	imgLoader.DisplayImage(image_url, banner);
			} catch (OutOfMemoryError e) {
				if ( e != null) {
					Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(MyMemberShipCardActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			MyMemberShipCardActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onPause() {
        super.onPause();
        try {
            System.gc();
            Runtime.getRuntime().gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
     }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			unRegisterBaseActivityReceiver();
			Runtime.getRuntime().gc();
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}	
	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		try {
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}
}
