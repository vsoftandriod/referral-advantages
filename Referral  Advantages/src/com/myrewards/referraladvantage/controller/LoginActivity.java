package com.myrewards.referraladvantage.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.myrewards.referraladvantage.cache.WebImageCache;
import com.myrewards.referraladvantage.model.LoginDetails;
import com.myrewards.referraladvantage.model.User;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.timer.CrittercismAndroid;
import com.myrewards.referraladvantage.utils.AnimeUtils;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;
import com.myrewards.referraladvantage.xml.FirsttimeLoginParser;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;

@SuppressLint({ "SimpleDateFormat", "ShowToast" })
public class LoginActivity extends Activity implements OnClickListener,
		ServiceListener, AnimationListener, OnLongClickListener {
	public static EditText etUserId;
	public static EditText etPasswd;
	RelativeLayout parentRL;
	public static EditText subDomainURL_et;
	RelativeLayout parentSubLayout;
	ArrayAdapter<String> adapter;
	Button btnLogin, firstTimeLoginBtn;
	private InputMethodManager imm;
	DatabaseHelper dbHelper;
	ImageView splashIV;
	public static EditText msNoet;
	public static EditText webAdds;
	// public static AlertDialog dialogDetails2;
	SimpleDateFormat formatter;
	Dialog dialog;

	// public Animation hyperspaceJump;
	public Animation movement5;
	final private static int LOGIN_BUTTON = 1;
	// @SuppressWarnings("unused")
	// final private static int MEMBERSHIP_NO = 2;
	final private static int USERNAME_FIELD = 2;
	final private static int PASSWORD_FIELD = 3;
	final private static int NO_NETWORK_CON = 4;
	final private static int ALL_FIELDS_REQUIRED = 5;

	// Membership validation
	public TextView tv12;
	public Button okbutton;
	LoginDetails loginDetails;
	RelativeLayout loadingPanel;
	Bundle bundle = null;
	boolean finishState;

	public void deleteCmpleteCacheData()
	{
		try{
			deleteCache(this);
			WebImageCache cache=new WebImageCache(this);
			cache.clear();
			}
			catch(Exception e)
			{
				if(e!=null)
				{
					e.printStackTrace();
				}
			}
	}
	public static void deleteCache(Context context) {
	    try {
	        File dir = context.getCacheDir();
	        if (dir != null && dir.isDirectory()) {
	            deleteDir(dir);
	        }
	    } catch (Exception e) {}
	}
	public static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);

		deleteCmpleteCacheData();
		
		// Push Notifications Adding...................
		// Parse.initialize(this, YOUR_APPLICATION_ID, YOUR_CLIENT_KEY);
		Parse.initialize(this, "9wNUlOV8mLeTZsg878tv1r5lR1u4w3CI07SkvAyR", "DFwZgRF7FcrAECyPOdTLOXrF8tbsSafLgPq3kWdw");

		PushService.setDefaultPushCallback(this, AppPushNotificationActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		ParseAnalytics.trackAppOpened(getIntent());

		setDimensions();
		ApplicationConstants.USERNAME_PASSWORD_FIELDS = getResources()
				.getString(R.string.uname_paswd_app);
		ApplicationConstants.UNABLETOESTABLISHCONNECTION_URL = getResources()
				.getString(R.string.invalid_url_estb);
		ApplicationConstants.UNABLETOESTABLISHCONNECTION = getResources()
				.getString(R.string.unable_to_estb);
		dbHelper = new DatabaseHelper(this);
		etUserId = (EditText) findViewById(R.id.usernameETID);

		etUserId.setTypeface(Utility.font_reg);

		etUserId.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		etPasswd = (EditText) findViewById(R.id.passwordETID);
		etPasswd.setTypeface(Utility.font_reg);

		etPasswd.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		subDomainURL_et = (EditText) findViewById(R.id.subDomainETID);
		subDomainURL_et.setTypeface(Utility.font_reg);

		subDomainURL_et.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		btnLogin = (Button) findViewById(R.id.loginBtnID);
		btnLogin.setTypeface(Utility.font_bold);
		btnLogin.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		btnLogin.setOnClickListener(this);

		etUserId.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		etPasswd.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		
		
		
		subDomainURL_et.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
				return false;
			}
		});
		//etUserId.setText("m1");
		//etPasswd.setText("rewards009");
		//subDomainURL_et.setText("www.motoronerewards.com.au");
		
		etUserId.setOnLongClickListener(this);
		etPasswd.setOnLongClickListener(this);
		subDomainURL_et.setOnLongClickListener(this);
		
		GPSTracker mGPS = new GPSTracker(this);
		if (mGPS.canGetLocation) {
			double mLat = mGPS.getLatitude();
			double mLong = mGPS.getLongitude();
			Utility.mLat = mLat;
			Utility.mLng = mLong;

			Log.v("mLatmLatmLat", "=" + mLat);
			Log.v("mLongmLongmLongmLong", "=" + mLong);

		} else {
			// can't get the location
		}
		splashIV = (ImageView) findViewById(R.id.splashIVID);
		loadingPanel = (RelativeLayout) findViewById(R.id.loadingPanelNew);
		splashIV.setVisibility(ImageView.GONE);

		try {
			loginDetails = dbHelper.getLoginDetails();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
		}
		Date loginDate = null;
		Date currentDate = null;
		// Date Format: MMM DD, YYYY hh:mm:ss ex: Mar 11, 2013 10:03:08 PM
		if (loginDetails != null) {
			try {
				formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				try {
					loginDate = formatter.parse(loginDetails.getLoginDate());
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				Calendar cal = Calendar.getInstance();
				String currentDateString = dateFormat.format(cal.getTime());
				try {
					currentDate = formatter.parse(currentDateString);
				} catch (ParseException e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}

				long diff = currentDate.getTime() - loginDate.getTime();
				long days = diff / (1000 * 60 * 60 * 24);
				if (days < 7) {
					splashIV.setVisibility(ImageView.GONE);
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						GrabItNowService.getGrabItNowService().sendLoginRequest(this, loginDetails.getUsername(),loginDetails.getPassword(),loginDetails.getSubDomain());
					} else {
						showDialog(NO_NETWORK_CON);
					}
				} else {
					try {
					     dbHelper.deleteLoginDetails();
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.v("Hari-->", e.getMessage());
					   }
					}
					loginDetails=null;
					splashIV.setVisibility(ImageView.GONE);
					loadingPanel.setVisibility(View.GONE);
					// Animation //
					parentSubLayout = (RelativeLayout) findViewById(R.id.loginRLID);
					parentSubLayout.setVisibility(View.VISIBLE);
					// movement5 =
					
					movement5 = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.accelerate_one);
					movement5.reset();
					movement5.setFillAfter(true);
					movement5.setAnimationListener(this);
					parentSubLayout.startAnimation(movement5);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->DEBUG", e);
				}
			}
		} else {
			try {
				splashIV.setVisibility(ImageView.GONE);
				loadingPanel.setVisibility(View.GONE);
				// Animation //
				parentSubLayout = (RelativeLayout) findViewById(R.id.loginRLID);
				parentSubLayout.setVisibility(View.VISIBLE);
				// movement5 =
				// AnimationUtils.loadAnimation(this,R.anim.animation_test5_forlogin);
				movement5 = AnimationUtils.loadAnimation(this, R.anim.accelerate_one);
				movement5.reset();
				movement5.setFillAfter(true);
				movement5.setAnimationListener(this);
				parentSubLayout.startAnimation(movement5);
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->DEBUG", e);
				}
			}
		}

		firstTimeLoginBtn = (Button) findViewById(R.id.firstTimeloginBtnID);
		firstTimeLoginBtn.setTypeface(Utility.font_bold);
		firstTimeLoginBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20);
		firstTimeLoginBtn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				showDialog(LOGIN_BUTTON);
			}
		});

		/*try {
			Drawable d;
			d = AnimeUtils.loadDrawableFromResource(getResources(),	R.drawable.sample1);
			((ImageView) findViewById(R.id.imageView1)).setImageDrawable(d);

			String path = Environment.getExternalStorageDirectory()
					+ "/sample1.gif";
			d = AnimeUtils.loadDrawableFromFile(getResources(), path);
		} catch (OutOfMemoryError e) {
			Toast.makeText(this, "Server Busy !", 2000).show();
		}*/
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		try {
			// start/stop animation
			AnimeUtils.startViewAnimation(findViewById(R.id.imageView1), hasFocus);
			AnimeUtils.startViewAnimation(findViewById(R.id.loginRLID),	hasFocus);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	// custom dialogs of first time login page
	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog dialogDetails = null;
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(R.layout.dialog_layout, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				return dialogDetails;
			} else if (id == 2) {
				AlertDialog dialogDetails2 = null;
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_username_field_login_page, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				dialogDetails2.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails2.show();
				return dialogDetails2;
			} else if (id == 3) {
				AlertDialog dialogDetails3 = null;
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(
						R.layout.dialog_password_field_login_page, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails3 = dialogbuilder.create();
				dialogDetails3.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails3.show();
				return dialogDetails3;
			} else if (id == 4) {
				AlertDialog noNetworkDialog = null;
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				return noNetworkDialog;
			} else if (id == 5) {
				AlertDialog noNetworkDialog = null;
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_all_fields_required, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				return noNetworkDialog;
			}
		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog1) {
		try {

			dialog = dialog1;
			if (id == 1) {
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				TextView alertMessage = (TextView) alertDialog1
						.findViewById(R.id.alertMessageID);
				alertMessage.setTypeface(Utility.font_reg);

				Button loginbutton = (Button) alertDialog1
						.findViewById(R.id.submitBtn);

				Button cancelbutton = (Button) alertDialog1
						.findViewById(R.id.btn_cancel);
				loginbutton.setTypeface(Utility.font_bold);
				cancelbutton.setTypeface(Utility.font_bold);
				msNoet = (EditText) alertDialog1
						.findViewById(R.id.membershipCardETID);
				msNoet.setTypeface(Utility.font_reg);

				webAdds = (EditText) alertDialog1.findViewById(R.id.webAddressETID);
				
				webAdds.setVisibility(View.GONE);
				
				webAdds.setTypeface(Utility.font_reg);

				msNoet.setOnLongClickListener(this);
				webAdds.setOnClickListener(this);

				loginbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (msNoet.getText().toString() != null) {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								GrabItNowService.getGrabItNowService().sendFirstTimeLoginDetails(LoginActivity.this, msNoet.getText().toString(), ApplicationConstants.subDomainURLString);
								//alertDialog1.dismiss();
							} else {
								showDialog(NO_NETWORK_CON);
							}
						}
					}
				});
				cancelbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
			} else if (id == 2) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				TextView alertMessage = (TextView) alertDialog2
						.findViewById(R.id.userIDInvalidTVID);
				alertMessage.setTypeface(Utility.font_reg);
				tv12 = (TextView) alertDialog2.findViewById(R.id.userIDInvalidTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			} else if (id == 3) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);
				TextView alertMessage = (TextView) alertDialog2
						.findViewById(R.id.passwordInvalidTVID);
				alertMessage.setTypeface(Utility.font_reg);

				tv12 = (TextView) alertDialog2
						.findViewById(R.id.passwordInvalidTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			} else if (id == 4) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2.findViewById(R.id.noConnTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			} else if (id == 5) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv12 = (TextView) alertDialog2.findViewById(R.id.allFieldsTVID);
				tv12.setTypeface(Utility.font_reg);
				okbutton = (Button) alertDialog2
						.findViewById(R.id.allFieldsOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
						if (parentSubLayout.getVisibility() == View.GONE) {
							parentSubLayout.setVisibility(View.VISIBLE);
							loadingPanel.setVisibility(View.GONE);
							try {
								if (dbHelper != null) {
									dbHelper.deleteLoginDetails();
									loginDetails = dbHelper.getLoginDetails();
								}
							} catch (Exception e) {
								if (e != null) {
									e.printStackTrace();
									Log.w("HARI-->DEBUG", e);
								}
							}
						}
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.loginBtnID:
			try {
				String userId = etUserId.getText().toString();
				String passwd = etPasswd.getText().toString();
			//	String subDomainURL = subDomainURL_et.getText().toString();

				String subDomainURL =ApplicationConstants.subDomainURLString;
				
				if (userId != null && userId.trim().length() > 0) {
					if (passwd != null && passwd.trim().length() > 0) {
						try {
							issueRequest(userId, passwd, subDomainURL);
						} catch (Exception e) {
							if (e != null) {
								Log.w("HARI-->DEBUG", e);
							}
						}
					} else {
						showDialog(PASSWORD_FIELD);
						btnLogin.setEnabled(true);
					}
				} else {
					showDialog(USERNAME_FIELD);
					btnLogin.setEnabled(true);
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
			break;
		}
	}

	private void issueRequest(String userId, String password,
			String subDomainURL) {
		try {
			imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(etUserId.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(etPasswd.getWindowToken(), 0);
			System.out.println("password: " + password);
			if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
				GrabItNowService.getGrabItNowService().sendLoginRequest(this, userId, password, subDomainURL);
				splashIV.setVisibility(ImageView.VISIBLE);
			} else {
				showDialog(NO_NETWORK_CON);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType == 1 || eventType == 2) {
				if (response != null) {
					if (response instanceof String) {
						showDialog(ALL_FIELDS_REQUIRED);
						splashIV.setVisibility(ImageView.GONE);
					} else {
						if (loginDetails == null) {
							try {
								DateFormat dateFormat = new SimpleDateFormat(
										"yyyy/MM/dd HH:mm:ss");
								Calendar cal = Calendar.getInstance();

								dbHelper.addLoginDetails(etUserId.getText()
										.toString(), etPasswd.getText()
										.toString(), ApplicationConstants.subDomainURLString, dateFormat.format(cal
										.getTime()));
							} catch (Exception e) {
								if (e != null) {
									e.printStackTrace();
									Log.w("HARI-->DEBUG", e);
								}
							}
							Utility.user_Name = etUserId.getText().toString();
							Utility.user_Password = etPasswd.getText().toString();
							//Utility.user_website = subDomainURL_et.getText().toString();
							Utility.user_website =ApplicationConstants.subDomainURLString;
						}
						if (Utility.user_Name == null) {
							Utility.user_Name = dbHelper.getLoginDetails().getUsername();
							Utility.user_Password = dbHelper.getLoginDetails().getPassword();
							Utility.user_website = dbHelper.getLoginDetails().getSubDomain();
						}
						Utility.user = (User) response;

						try {
							if (dbHelper.getLoginDetails() != null) {
								if (dbHelper.getLoginDetails().getUsername() != null) {
									String uName = dbHelper.getLoginDetails().getUsername().toString();
									CrittercismAndroid.SetUsername(uName);
								}
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("Hari--> Login Page", e);
							}
						}

						Intent i = new Intent(LoginActivity.this, SearchListActivity.class);
						i.putExtra("LOGIN_ACTIVITY", "LOGIN_ACTIVITY");
						startActivity(i);
						LoginActivity.this.finish();
					}
				}
			} else if (eventType == 14) {
				if (response != null) {

					if (FirsttimeLoginParser.firsttimeloginresponce.equalsIgnoreCase("TRUE"))

					{
						Animation shake = AnimationUtils.loadAnimation(
								LoginActivity.this, R.anim.shake);
						msNoet.startAnimation(shake);
						webAdds.startAnimation(shake);
					} else if (FirsttimeLoginParser.firsttimeloginresponce
							.equalsIgnoreCase("FALSE")) {
						Intent in = new Intent(this,
								FirstTimeLoginActivity.class);
						in.putExtra("STR1", msNoet.getText().toString());
						in.putExtra("STR2", ApplicationConstants.subDomainURLString);
						startActivity(new Intent(LoginActivity.this,
								FirstTimeLoginActivity.class));
						dialog.dismiss();
					}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void setDimensions() {
		try {
			Display display = getWindowManager().getDefaultDisplay();
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			Utility.screenWidth = screenWidth;
			Utility.screenHeight = screenHeight;
			parentRL = (RelativeLayout) findViewById(R.id.loginParentRLID);
			parentSubLayout = (RelativeLayout) findViewById(R.id.loginRLID);
		
			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(Utility.screenWidth / 10,0,
					Utility.screenWidth / 10, Utility.screenWidth /32);
			params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM );
			params.addRule(RelativeLayout.CENTER_HORIZONTAL);
			parentSubLayout.setLayoutParams(params);
			
			Utility.font_bold = Typeface.createFromAsset(this.getAssets(),"helvetica_bold.ttf");
			Utility.font_reg = Typeface.createFromAsset(this.getAssets(),"helvetica_reg.ttf");
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	protected void onRestart() {
		splashIV.setVisibility(ImageView.GONE);
		super.onRestart();
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);

		} catch (Exception e) {
			returnValue = false;
		}
		return returnValue;
	}
}
