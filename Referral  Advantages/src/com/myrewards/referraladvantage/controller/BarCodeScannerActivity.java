package com.myrewards.referraladvantage.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.google.zxing.client.android.CaptureActivity;

public class BarCodeScannerActivity extends CaptureActivity
{
	@Override
	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		super.onKeyDown(keyCode, event);
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			try {
				Intent i = new Intent(BarCodeScannerActivity.this, SearchListActivity.class);
				i.putExtra("finish", true);
				i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
				startActivity(i);
				BarCodeScannerActivity.this.finish();
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
		}
		return true;
	}
}
