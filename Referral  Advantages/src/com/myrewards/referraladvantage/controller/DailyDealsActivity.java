package com.myrewards.referraladvantage.controller;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.Product;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.Utility;

public class DailyDealsActivity extends HomeBasedActivity implements
		ServiceListener { 
	Product product;
	View loading;
	SmartImageView dailyDealsIV;
	String imageURL;
	public static String headerTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.daily_deals_main);
		registerBaseActivityReceiver();
		
		Utility.FavouritesScreen ="null";
		Utility.hotOffersScreen = "null";
		Utility.AroundMeScreen = "null";
		Utility.ResultScreen ="null";
		
		try {
			headerTitle =getResources().getString(R.string.daily_deals_text);
			setHeaderTitle(headerTitle);
			
			RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
			headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			
			menuListView = (ListView) findViewById(R.id.menuListViewID);
			try {
				initialiseViews();
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI--> DEBUG", e);
				}
			}
			loading = (View) findViewById(R.id.loading);
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				GrabItNowService.getGrabItNowService().sendDailyDealsRequest(this);
				if (Utility.user != null) {
					if (Utility.user.getClientBanner() == null) {
						try {
							String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
							newImagesLoading(bannerURL);
						} catch (Exception e) {
							if ( e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
					} 
				}
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				DailyDealsActivity.this.finish();
				headerTitle = null;
			}
			dailyDealsIV = (SmartImageView) findViewById(R.id.dailyDealsIVID);
			dailyDealsIV.setEnabled(false);
			dailyDealsIV.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (menuListView.getVisibility() == ListView.GONE) {
							Intent detailsIntent = new Intent(DailyDealsActivity.this, ProductDetailsActivity.class);
							detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,	product.getId());
							detailsIntent.putExtra(ApplicationConstants.IMAGE_URL_KEY, imageURL);
							detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, 0);
							detailsIntent.putExtra(Utility.FINISHED_STATUS, "DAIYLY_DEALS_SCREEN");
							startActivity(detailsIntent);
							//DailyDealsActivity.this.finish();
						}
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));
									 
						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
						DailyDealsActivity.this.finish();
						headerTitle = null;
					}
				}
			});
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
		
	}
	
	@Override
	protected void onDestroy() {
	   super.onDestroy();
	   System.gc();
	   unRegisterBaseActivityReceiver();
	}

	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		//int loader = R.drawable.loading_imp;
		//int loader = banner.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = _bannerPath;
        Log.w("Hari-->", _bannerPath);
        
        // ImageLoader class instance
      //  BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        try {
        	banner.setImageUrl(image_url);
        	//imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
        loading.setVisibility(View.VISIBLE);
	}

	@Override
	public void onServiceComplete(Object response, int eventType) 
	{
		if (response != null) {
			if (response instanceof String) {
				// showErrorDialog(response.toString());
				//Utility.showMessage(this, response.toString());
			} else {
				try
				{
					if(eventType!=16 && eventType==13)
					{
				product = (Product) response;
				if (product.getId() != 0
						&& product.getHotoffer_extension() != null) {
					loading.setVisibility(View.VISIBLE);
					imageURL = ApplicationConstants.DAILY_DEALS_IMAGE_WRAPPER
							+ product.getId() + "."
							+ product.getHotoffer_extension();
					//ImageURLTask imgTask = new ImageURLTask();
					//imgTask.setListener(this, "DailyDealsImage");
					//imgTask.execute(imageURL);
					String dailyDeals = "DailyDealsImage";
					loading.setVisibility(View.VISIBLE);
					try {
						newCardImagesLoading(imageURL, dailyDeals);
					} catch (Exception e) {
						if (e != null) {
							Log.w("Hari-->DEBUG", e);
						}
					}
				}
					}
				}
				catch(Exception e)
				{
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
			}
		}
	}
	
	private void newCardImagesLoading(String cardURL, String _dailyDeals) {
		// Image url
        String image_url = cardURL;
        Log.w("Hari-->", cardURL);
        
        // ImageLoader class instance
    //   BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext(), _dailyDeals);
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
       loading.setVisibility(View.VISIBLE);
      // dailyDealsIV.setVisibility(View.VISIBLE);
       try {
    	  // imgLoader.DisplayImage(image_url, dailyDealsIV);
    	   dailyDealsIV.setImageUrl(image_url);
    		dailyDealsIV.setEnabled(true);
    		loading.setVisibility(View.GONE);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
       //if (dailyDealsIV.getBackground() != null) {
       dailyDealsIV.setVisibility(View.VISIBLE);
       dailyDealsIV.setEnabled(true);
       loading.setVisibility(View.GONE);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent i = new Intent(DailyDealsActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			headerTitle = null;
			DailyDealsActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
