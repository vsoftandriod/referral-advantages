package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.Product;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.Utility;

public class HotOffersActivity extends HomeBasedActivity implements ServiceListener
		 { //IImageURLTaskListener
	List<Product> hotOffersProductsList;
	HotOffersAdapter mAdapter;
	LayoutInflater inflater;
	View loading;
	static ListView hotOffersListView;
	String catID = null;
	String location = null;
	String keyword = null;
	
	public static String headerTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		registerBaseActivityReceiver();
		Utility.AroundMeScreen = "null";
		Utility.FavouritesScreen ="null";
		Utility.DailyDealsScreen ="null";
		Utility.ResultScreen ="null";
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		headerTitle = getResources().getString(R.string.hot_offers_text);
		setHeaderTitle(headerTitle);
		
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		hotOffersListView = (ListView) findViewById(R.id.resultsListViewID);//2131230975 //android.widget.ListView@42047278
		hotOffersProductsList = new ArrayList<Product>();
		hotOffersListView.setOnItemClickListener(this);
			
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			GrabItNowService.getGrabItNowService().sendHotOffersRequest(this);
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
			(ViewGroup) findViewById(R.id.custom_toast_layout_id));
						 
			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			HotOffersActivity.this.finish();
			headerTitle = null;
		}
	}
	
	@Override
	protected void onDestroy() {
	   super.onDestroy();
	   System.gc();
	   unRegisterBaseActivityReceiver();
	}

	public class HotOffersAdapter extends BaseAdapter {

		public HotOffersAdapter(HotOffersActivity hotOffersActivity) {

		}

		@Override
		public int getCount() {
			return hotOffersProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			try {
				View resultsListRow = null;
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(R.layout.results_list_item, null, false);
				}
				LinearLayout rowLL = (LinearLayout) resultsListRow.findViewById(R.id.resultItemLLID);
				LinearLayout rowLL2 = (LinearLayout)resultsListRow.findViewById(R.id.resultListItemHariLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
				TextView productNameTV = (TextView) resultsListRow.findViewById(R.id.productTVID);
				productNameTV.setTypeface(Utility.font_bold);
				
				TextView highlightTV = (TextView) resultsListRow.findViewById(R.id.offerTVID);
				highlightTV.setTypeface(Utility.font_reg);
				
				productNameTV.setText(hotOffersProductsList.get(pos).getName());
				highlightTV.setText(hotOffersProductsList.get(pos).getHighlight());
				
				
				
				
				
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				}
				return resultsListRow;
			
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->", e);
				}
				return null;
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,	long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			try {//2131230975 //android.widget.ListView@42047278
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					if (menuListView.getVisibility() == ListView.GONE) {
						Intent detailsIntent = new Intent(HotOffersActivity.this, ProductDetailsActivity.class);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY, hotOffersProductsList.get(pos).getId());
						detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 4);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY, hotOffersProductsList.get(pos).getName());
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY, hotOffersProductsList.get(pos).getHighlight());
						detailsIntent.putExtra(Utility.FINISHED_STATUS, "HOT_OFFERS_SCREEN");
						startActivity(detailsIntent);
					}
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try{
		if (response != null) {
			if (response instanceof String) {
				// showErrorDialog(response.toString());
			//	Utility.showMessage(this, response.toString());
			} else {
				if(eventType!=16 && eventType==5){
				hotOffersProductsList = (ArrayList<Product>) response;
				if (Utility.user.getClientBanner() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				} 
				mAdapter = new HotOffersAdapter(this);
				hotOffersListView.setAdapter(mAdapter);
				loading.setVisibility(View.GONE);
				}
			}
		}
	}catch(Exception e)
		{
		if (e != null) {
			e.printStackTrace();
			Log.w("HARI-->DEBUG", e);
		}
		}
	}
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		//int loader = R.drawable.loading_imp;
		//int loader = banner.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = _bannerPath;
        Log.w("Hari-->", _bannerPath);
        
        // ImageLoader class instance
     //   BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        try {
        	banner.setImageUrl(image_url);
        	 //imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent i = new Intent(HotOffersActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			HotOffersActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
}
