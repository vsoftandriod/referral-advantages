package com.myrewards.referraladvantage.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.Utility;

public class NoticeBoardDetailsActivity extends HomeBasedActivity {// implements
																// IImageURLTaskListener{
	View loading;
	String noticeId;
	String noticeSubject = null;
	String noticeDetails = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.notice_board_details);
		registerBaseActivityReceiver();

		setHeaderTitle(getResources().getString(R.string.notice_board_text));
		showBackButton();
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			if (Utility.user != null) {
				if (Utility.user.getMyMembershipCard() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				} 
			}
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
		}
		if (getIntent() != null) {
			int colorResource = 0;
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				noticeId = bundle.getString(ApplicationConstants.NOTICE_ID_KEY);
				noticeSubject = bundle.getString(ApplicationConstants.NOTICE_NAME_KEY);
				noticeDetails = bundle.getString(ApplicationConstants.NOTICE_DETAILS_KEY);
				colorResource = bundle.getInt(ApplicationConstants.COLOR_CODE_KEY);
			}
			View resultListItem = (View) findViewById(R.id.resultListItemID);
			ImageView arrowImage = (ImageView) findViewById(R.id.arrowIVID);
			arrowImage.setVisibility(View.GONE);
			switch (colorResource) {
			case 0:
				resultListItem.setBackgroundResource(R.color.result_color_one);
				break;
			case 1:
				resultListItem.setBackgroundResource(R.color.result_color_two);
				break;
			case 2:
				resultListItem.setBackgroundResource(R.color.result_color_three);
				break;
			case 3:
				resultListItem.setBackgroundResource(R.color.result_color_four);
				break;
			}
			
			TextView noticeNameTV = (TextView) findViewById(R.id.productTVID);
			noticeNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
			
			noticeNameTV.setTypeface(Utility.font_bold);
			if (noticeSubject != null) {
				noticeNameTV.setText(noticeSubject);
			}
			
			/*TextView noticeDetailsTV = (TextView) findViewById(R.id.noticeBoardDetailsTVID);
			noticeDetailsTV.setTypeface(Utility.font_reg);
			if (noticeDetails != null) {
				noticeDetailsTV.setText(Html.fromHtml(noticeDetails));
			}*/
			
			loadInWebView(noticeDetails);
		}
	}
	
	@Override
	protected void onDestroy() {
	   super.onDestroy();
	   try {
		   System.gc();
		   unRegisterBaseActivityReceiver();
		  // HariMemoryManager.unbindDrawables(findViewById(R.id.noticeDetailsRootRLID));
	   } catch (Exception e) {
		   if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
	   }
	}

	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		// bannerIV.getLayoutParams().width = Utility.screenWidth * 1;

		// int loader = R.drawable.loading_imp;
		// int loader = bannerIV.getWidth();
		// Log.w("Hari-->", String.valueOf(loader));
		// int loader = bannerIV.getLayoutParams().width = Utility.screenWidth;

		// Image url
		String image_url = _bannerPath;
		Log.w("Hari-->", _bannerPath);

		// ImageLoader class instance
		//BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView
		// imgLoader.DisplayImage(image_url, loader, bannerIV);
		try {
			banner.setImageUrl(image_url);
		//	imgLoader.DisplayImage(image_url, bannerIV);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backToSearchBtnID) {
			startActivity(new Intent(NoticeBoardDetailsActivity.this, NoticeBoardActivity.class));
			NoticeBoardDetailsActivity.this.finish();
		}
		super.onClick(v);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(NoticeBoardDetailsActivity.this, NoticeBoardActivity.class));
			NoticeBoardDetailsActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void loadInWebView(String webContent) {
		WebView webView = (WebView) findViewById(R.id.detailsTVID);
	
		webView.getSettings()
		.setJavaScriptEnabled(true);
webView.getSettings().setAllowFileAccess(true);
webView.getSettings()
		.setLoadsImagesAutomatically(true);
webView.getSettings().setUserAgentString(Locale.getDefault().getLanguage());
WebSettings settings = webView.getSettings();
settings.setDefaultTextEncodingName("utf-8");
webView.setWebViewClient(new WebViewClient());
webView.setWebViewClient(new WebViewClient() {

	@Override
	public boolean shouldOverrideUrlLoading(
			WebView view, String url) {
		if (url.startsWith("tel:")) {
			Intent intent = new Intent(
					Intent.ACTION_DIAL, Uri.parse(url));
			startActivity(intent);
		} else if (url.startsWith("http:")
				|| url.startsWith("https:")) {
			Intent intent = new Intent(
					Intent.ACTION_VIEW, Uri.parse(url));
			startActivity(intent);
		} else if (url.startsWith("mailto:")) {
			MailTo mt = MailTo.parse(url);
			Intent i = EmailIntent(NoticeBoardDetailsActivity.this,mt.getTo(), mt.getSubject(),
					mt.getBody(), mt.getCc());
			startActivity(i);
			view.reload();
			return true;
		} else {
			view.loadUrl(url);
		}
		return true;
	}
});
/*
 * String summary = Html.fromHtml(
 * product.getDetails() + "\n" + "\n" +
 * product.getText()).toString();
 */
String summary = "<html><body style=\"font-family:Helvetica;line-height:20px\">"
		+ webContent
		+ "</body></html>";

summary = summary.replaceAll("//", "");
// create text file
if (!Environment.getExternalStorageState()
		.equals(Environment.MEDIA_MOUNTED))
	Log.d("GIN", "No SDCARD");
else {
	File direct = new File(
			Environment
					.getExternalStorageDirectory()
					+ "/GIN");

	if (!direct.exists()) {
		if (direct.mkdir()) {
			// directory is created;
		}
	}

	try {
		File root = new File(
				Environment
						.getExternalStorageDirectory()
						+ "/GIN");
		if (root.canWrite()) {
			File file = new File(root,
					"GINnoticedetails.html");
			FileWriter fileWriter = new FileWriter(
					file);
			BufferedWriter out = new BufferedWriter(
					fileWriter);
			if (summary.contains("<iframe")) {
				try {
					int a = summary
							.indexOf("<iframe");
					int b = summary
							.indexOf("</iframe>");
					summary = summary
							.replace(
									summary.subSequence(
											a,
											b),
									"");
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						
					}
				}
			}
			out.write(summary);
			out.close();
		}
	} catch (IOException e) {
		if (e != null) {
			e.printStackTrace();
		
		}
	}
	
	if (!Environment.getExternalStorageState()
			.equals(Environment.MEDIA_MOUNTED)) {
		Log.d("GIN", "No SDCARD");
	} else {

		webView.loadUrl("file://"
				+ Environment
						.getExternalStorageDirectory()
				+ "/GIN"
				+ "/GINnoticedetails.html");
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onReceivedError(
					WebView view, int errorCode,
					String description,
					String failingUrl) {
				Log.i("WEB_VIEW_TEST",
						"error code:" + errorCode);

				super.onReceivedError(view,
						errorCode, description,
						failingUrl);
			}
		});

		
	}
}
}
	public static Intent EmailIntent(Context context, String address,
			   String subject, String body, String cc) {
			  Intent intent = new Intent(Intent.ACTION_SEND);
			  intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
			  intent.putExtra(Intent.EXTRA_TEXT, body);
			  intent.putExtra(Intent.EXTRA_SUBJECT, subject);
			  intent.putExtra(Intent.EXTRA_CC, cc);
			  intent.setType("message/rfc822");
			  return intent;
			 }
}
