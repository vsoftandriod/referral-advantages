package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.myrewards.referraladvantage.model.NoticeId;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

public class NoticeBoardReceiver extends BroadcastReceiver implements
		ServiceListener {
	ArrayList<NoticeId> noticeid;
	DatabaseHelper dbHelper;
	Context context;
	String noticeMessages = "";

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			this.context = context;
			dbHelper = new DatabaseHelper(context);
			if (Utility.isOnline((ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE)))
				if (Utility.isOnline((ConnectivityManager) context
						.getSystemService(Context.CONNECTIVITY_SERVICE))) {
					GrabItNowService.getGrabItNowService()
							.sendNoticeBoardCountIdRequset(this);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = ((Activity) context)
							.getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) ((Activity) context)
									.findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(context.getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void sendNotification() {
		try {
			final NotificationManager manager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			final Notification msg = new Notification(R.drawable.my_notice,
					"Noticeboard Updates", System.currentTimeMillis());
			msg.sound = RingtoneManager
					.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			;
			Intent in = new Intent(context, SearchListActivity.class);

			PendingIntent pin = PendingIntent.getActivity(context, 0, in, 0);
			msg.setLatestEventInfo(context, "Noticeboard Updates",
					"New Message From Noticeboard !", pin);
			msg.flags = Notification.FLAG_AUTO_CANCEL;
			manager.notify(1, msg);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType == 16) {
				if (response != null) {
					try {
						noticeid = (ArrayList<NoticeId>) response;
						int i = -1;
						if (noticeid != null)
						{
							List<String> updateNoticesList = new ArrayList<String>();
							for (int n = 0; n < noticeid.size(); n++) {
								if (!(noticeid.get(n).getNoticeDetails() == null))
									updateNoticesList.add(noticeid.get(n)
											.getNoticeDetails());
							}
							if (updateNoticesList.size() != dbHelper.getExistingIDs()
									.size()) {
								for (int m = 0; m < dbHelper.getExistingIDs()
										.size(); m++) {
									if (updateNoticesList.contains(dbHelper
											.getExistingNoticeRealIDs().get(m))) {

									} else {
										dbHelper.deleteNoticeIdDetails(dbHelper
												.getExistingNoticeRealIDs().get(m));

									}
								}
								if (updateNoticesList.size() > dbHelper.getExistingIDs()
										.size()) {

									sendNotification();
								}
							}

							for (int k = 0; k < noticeid.size(); k++) {
								if (noticeid.get(k).getNoticeDetails() != null) {
									i++;
									List<String> list = dbHelper.getExistingIDs();
									if (!list.contains(noticeid.get(k)
											.getNoticeDetails())) {
										dbHelper.addnoticeiddetails(noticeid.get(k)
												.getNoticeDetails());
									}
								}
							}
							Log.v("Hari---->", "Count = "+i);
						}
					} catch (Exception e) {
						e.printStackTrace();
						
					}
				}
			}

		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

}
