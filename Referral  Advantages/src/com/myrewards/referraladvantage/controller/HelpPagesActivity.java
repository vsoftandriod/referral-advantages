package com.myrewards.referraladvantage.controller;

import java.util.List;
import java.util.Vector;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.HelpPageFive;
import com.myrewards.referraladvantage.model.HelpPageFour;
import com.myrewards.referraladvantage.model.HelpPageOne;
import com.myrewards.referraladvantage.model.HelpPageSix;
import com.myrewards.referraladvantage.model.HelpPageThree;
import com.myrewards.referraladvantage.model.HelpPageTwo;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

/** 
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity
 * hosting the ViewPager
 * 
 * @author Hari
 */
public class HelpPagesActivity extends HomeBasedActivity implements
		OnClickListener, OnItemClickListener,
		AnimationListener { //IImageURLTaskListener
	/** maintains the pager adapter */
	private PagerAdapter mPagerAdapter;
	private View loading;
	@SuppressWarnings("unused")
	private boolean isClientLogo = false;
	LayoutInflater inflater;
	DatabaseHelper helper;
	public static boolean noticeboardcount = false;
	static int noticeCount = 0;
	public ImageView noticeboardunread;

	// logout
	public TextView logoutText;
	public Button loginbutton1, cancelbutton1;

	// no favorite found
	public TextView noFavTV;
	public Button noFavOKBtn;
	public static String headerTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		registerBaseActivityReceiver();
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		menuBtn.setOnClickListener(this);

		initialiseViews();
		helper = new DatabaseHelper(HelpPagesActivity.this);
		headerTitle = getResources().getString(R.string.help_title_text);  
		setHeaderTitle(headerTitle);

		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		
		try {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (Utility.user != null) {
					if (Utility.user.getClientBanner() == null) {
						String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
						try {
							newImagesLoading(bannerURL);
						} catch (Exception e) {
							if (e != null) {
								Log.w("HARI-->DEBUG", e);
							}
						}
					} 
			} else {
				// The Custom Toast Layout Imported here
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
							 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				HelpPagesActivity.this.finish();
				headerTitle = null;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
		try
		{
		// initialsie the pager
		this.initialisePaging();
		}
		catch(Exception e)
		{
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			System.gc();
			Runtime.getRuntime().gc();
			unRegisterBaseActivityReceiver();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}
	
	@Override
	protected void onPause() {
        super.onPause();
        try {
             System.gc();
             Runtime.getRuntime().gc();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
     }
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		//int loader = R.drawable.loading_imp;
		//int loader = banner.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = _bannerPath;
        Log.w("Hari-->", _bannerPath);
        
        // ImageLoader class instance
     //   BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        try {
        	banner.setImageUrl(image_url);
       // 	imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		try {
			List<Fragment> fragments = new Vector<Fragment>();
			fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
			fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
			this.mPagerAdapter = new PagerAdapter(super.getSupportFragmentManager(), fragments);
			ViewPager pager = (ViewPager) super.findViewById(R.id.viewpager);
			pager.setAdapter(this.mPagerAdapter);
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari--> DEBUG", e);
			}
		}
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent i = new Intent(HelpPagesActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			HelpPagesActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
}
