package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.NoticeBoard;
import com.myrewards.referraladvantage.model.NoticeId;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

@SuppressLint("ResourceAsColor")
public class NoticeBoardActivity extends HomeBasedActivity implements
		ServiceListener { // IImageURLTaskListener
	List<NoticeBoard> noticeBoardProductsList;
	NoticeBoardAdapter mAdapter;
	LayoutInflater inflater;
	public static int count1 = 0, count2 = 0, count3 = 0;
	View loading;
	ListView hotOffersListView;
	public static boolean noticecount = false;
	String catID = null;
	String location = null;
	String keyword = null;
	Boolean abc;

	ArrayList<NoticeId> noticeid;

	ImageView noticeArrow;

	ImageView noticeImage;
	ToggleButton tButton;

	public static String headerTitle = null;
	TextView noNoticesText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		try {
			registerBaseActivityReceiver();
			helper = new DatabaseHelper(this);
			headerTitle = getResources().getString(R.string.notice_board_text);
			setHeaderTitle(headerTitle);
			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			menuListView = (ListView) findViewById(R.id.menuListViewID);
			initialiseViews();
			loading = (View) findViewById(R.id.loading);
			hotOffersListView = (ListView) findViewById(R.id.resultsListViewID);

			noNoticesText = (TextView) findViewById(R.id.noFavAdded_deleteTVID);
			noNoticesText.setText(getResources().getString(R.string.no_notices_text));
			noticeBoardProductsList = new ArrayList<NoticeBoard>();
			hotOffersListView.setOnItemClickListener(this);

			try {
				if (Utility
						.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					GrabItNowService.getGrabItNowService()
							.sendNoticeBoardRequest(this);
				} else {
					// The Custom Toast Layout Imported here --> by HARI
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater
							.inflate(
									R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
					NoticeBoardActivity.this.finish();
					headerTitle = null;
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}

			RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
			headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

			RelativeLayout noticeUpdateRL = (RelativeLayout) findViewById(R.id.noticeUpdateRLID);
			noticeUpdateRL.setVisibility(View.VISIBLE);

			TextView noticeUpdateText = (TextView) findViewById(R.id.noticeUpdateTVID);
			noticeUpdateText.setTypeface(Utility.font_bold);

			tButton = (ToggleButton) findViewById(R.id.toggleButton1);

			tButton.getLayoutParams().width = (int) (Utility.screenWidth / 7);
			tButton.getLayoutParams().height = (int) (Utility.screenHeight / 18.0);
			try {
				if (helper.getNoticeUpdateState().equals("ON"))
					tButton.setChecked(true);
				else
					tButton.setChecked(false);
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
			tButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					try {
						if (isChecked) {
							helper.setNoticeUpdateState("ON");
							Utility.setNotificationReceiver(NoticeBoardActivity.this);
						} else {
							helper.setNoticeUpdateState("OFF");
							Utility.cancelNotificationReceiver();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("Hari-->DEBUG", e);
						}
					}
				}
			});
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			unRegisterBaseActivityReceiver();
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	public class NoticeBoardAdapter extends BaseAdapter {

		public NoticeBoardAdapter(NoticeBoardActivity noticeBoardActivity) {

		}

		@Override
		public int getCount() {
			return noticeBoardProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			try {
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(
							R.layout.results_list_item_two, null, false);
				}
				LinearLayout rowLL = (LinearLayout) resultsListRow
						.findViewById(R.id.resultItemLLID);
				TextView productNameTV = (TextView) resultsListRow
						.findViewById(R.id.productTVID);
				productNameTV.setTypeface(Utility.font_bold);
				TextView highlightTV = (TextView) resultsListRow
						.findViewById(R.id.offerTVID);
				highlightTV.setTypeface(Utility.font_reg);
				highlightTV.setVisibility(View.GONE);

				noticeImage = (ImageView) resultsListRow
						.findViewById(R.id.imageView1);
				noticeArrow = (ImageView) resultsListRow
						.findViewById(R.id.arrowIVID);

				productNameTV.setText(noticeBoardProductsList.get(pos)
						.getSubject());
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				}
				try {
					if (helper.getNoticeIdReadorUnread(
							noticeBoardProductsList.get(pos).getId()).equals(
							"CLOSE")) {
						noticeImage.setImageResource(R.drawable.notice_read);
					} else if (helper.getNoticeIdReadorUnread(
							noticeBoardProductsList.get(pos).getId()).equals(
							"OPEN")) {
						noticeImage.setImageResource(R.drawable.notice_unread);
					}
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
				return resultsListRow;
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
				return resultsListRow;
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			noticecount = true;
			try {
				if (menuListView.getVisibility() == ListView.GONE) {
					helper = new DatabaseHelper(this);
					helper.updateNoticeDetails(noticeBoardProductsList.get(pos)
							.getId());
					Intent detailsIntent = new Intent(NoticeBoardActivity.this,
							NoticeBoardDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.NOTICE_ID_KEY,
							noticeBoardProductsList.get(pos).getId());
					detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,
							pos % 4);
					detailsIntent.putExtra(
							ApplicationConstants.NOTICE_NAME_KEY,
							noticeBoardProductsList.get(pos).getSubject());
					detailsIntent.putExtra(
							ApplicationConstants.NOTICE_DETAILS_KEY,
							noticeBoardProductsList.get(pos).getDetails());
					startActivity(detailsIntent);
				}
			} catch (NullPointerException e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType != 16) {
				if (response != null) {
					if (response instanceof String) {
						// showErrorDialog(response.toString());
						// Utility.showMessage(this, response.toString());
					} else {

						if (eventType == 12) {
							noticeBoardProductsList = (ArrayList<NoticeBoard>) response;
							if (Utility.user != null) {
								if (Utility.user.getClientBanner() == null) {
									// ImageURLTask imgTask = new
									// ImageURLTask();
									// imgTask.setListener(this);
									// imgTask.execute(ApplicationConstants.CLIENT_BANNER_WRAPPER+
									// Utility.user.getClient_id());
									String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER
											+ Utility.user.getClient_id();
									newImagesLoading(bannerURL);
								} else {
									// onImageLoadComplete(Utility.user.getClientBanner());
								}
							}
							mAdapter = new NoticeBoardAdapter(this);
							hotOffersListView.setAdapter(mAdapter);

							// here shambhi logic goes

							if (noticeBoardProductsList.size() == 0) {
								noNoticesText.setVisibility(View.VISIBLE);
								hotOffersListView.setVisibility(View.GONE);
							}

						}
					}
				}
				updateNoticeBoardsDetails();
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;

		// int loader = R.drawable.loading_imp;
		// int loader = banner.getLayoutParams().width = Utility.screenWidth;

		// Image url
		String image_url = _bannerPath;
		Log.w("Hari-->", _bannerPath);

		// ImageLoader class instance
		/*BannerImageLoader imgLoader = new BannerImageLoader(
				getApplicationContext());*/

		// whenever you want to load an image from url
		// call DisplayImage function
		// url - image url to load
		// loader - loader image, will be displayed before getting image
		// image - ImageView

		try {
			banner.setImageUrl(image_url);
			//imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	private void updateNoticeBoardsDetails() {
		try {
			noticeid = new ArrayList<NoticeId>();
			for (int i = 0; i < noticeBoardProductsList.size(); i++) {
				noticeid.add(new NoticeId(noticeBoardProductsList.get(i)
						.getId()));
			}
			if (noticeid != null) {
				List<String> nUpdatesList = new ArrayList<String>();
				for (int n = 0; n < noticeid.size(); n++) {
					if (!(noticeid.get(n).getNoticeDetails() == null))
						nUpdatesList.add(noticeid.get(n).getNoticeDetails());
				}
				if (nUpdatesList.size() != helper.getExistingIDs().size()) {
					for (int m = 0; m < helper.getExistingIDs().size(); m++) {
						if (nUpdatesList.contains(helper
								.getExistingNoticeRealIDs().get(m))) {

						} else {
							helper.deleteNoticeIdDetails(helper
									.getExistingNoticeRealIDs().get(m));
						}
					}
				}

				for (int k = 0; k < noticeid.size(); k++) {
					if (noticeid.get(k).getNoticeDetails() != null) {

						List<String> list = helper.getExistingIDs();
						if (!list.contains(noticeid.get(k).getNoticeDetails())) {
							helper.addnoticeiddetails(noticeid.get(k)
									.getNoticeDetails());
						}
					}
				}
			}
			loading.setVisibility(View.GONE);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(NoticeBoardActivity.this,
					SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all
														// activities
			startActivity(i);
			NoticeBoardActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
}
