package com.myrewards.referraladvantage.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.myrewards.referraladvantage.utils.Utility;

public class AlertDialogActivity extends Activity {
	private static final int MY_PARKING_TIMER_EXPIRE = 1111;
	TextView tv111;
	Button okBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			Settings.System.putInt(getContentResolver(), Settings.System.SOUND_EFFECTS_ENABLED, 1);
			displayAlert();
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	private void displayAlert() {
		showDialog(MY_PARKING_TIMER_EXPIRE);
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1111) {
				AlertDialog myParkingExpireDialog = null;
				LayoutInflater lif = LayoutInflater.from(this);
				View expireView = lif.inflate(
						R.layout.dialog_layout_my_parking_timer_alert, null);
				AlertDialog.Builder adbMyParking = new AlertDialog.Builder(this);
				adbMyParking.setView(expireView);
				myParkingExpireDialog = adbMyParking.create();
				myParkingExpireDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				myParkingExpireDialog.show();
				return myParkingExpireDialog;
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
			return null;
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
			return null;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1111) {
				final AlertDialog alert2 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alert2
						.findViewById(R.id.myParkingTimerCEPUTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv111 = (TextView) alert2
						.findViewById(R.id.expire_my_parkingTVID);

				tv111.setTypeface(Utility.font_reg);

				okBtn = (Button) alert2.findViewById(R.id.expireOKBtnID);
				okBtn.setTypeface(Utility.font_bold);
				okBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alert2.dismiss();
						try {
							NewParkingTimeActivity.ReminderSet = false;
							NewParkingTimeActivity.setTime = 0;
							NewParkingTimeActivity.setTimeBtn
									.setText(getResources().getString(
											R.string.set_time));
							NewParkingTimeActivity.mAlarmApplication
									.stopTimer();
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						finish();
					}
				});
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}
}