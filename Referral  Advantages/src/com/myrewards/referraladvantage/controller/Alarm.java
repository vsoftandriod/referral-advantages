package com.myrewards.referraladvantage.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class Alarm extends BroadcastReceiver{
	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			this.context=context;
			Log.v("HAI", "ya its working");
			Intent in=new Intent(context, AlertDialogActivity.class);
			in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			String text=context.getResources().getString(R.string.alarm_recvd);
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			context.startActivity(in);
		} catch (NullPointerException _e) {
			if (_e != null) {
				_e.printStackTrace();
				Log.w("HARI-->DEBUG", _e);
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}
}
