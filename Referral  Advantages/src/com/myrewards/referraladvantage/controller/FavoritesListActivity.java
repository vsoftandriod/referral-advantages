package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.Product;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

public class FavoritesListActivity extends HomeBasedActivity {//implements IImageURLTaskListener {
	List<Product> myFavoriteProductsList;
	List<Product> hotOffersProductsList;
	Product product;

	MyFavoritesAdapter mAdapter;
	LayoutInflater inflater;
	View loading;
	ListView favoritesListView;
	ListView hotOffersListView;
	DatabaseHelper dbHelper;
	public static int position;
	private final int DELETE_MY_FAVORITE_BUTTON = 1;
	// private final int DELETE_MY_FAVORITE_BUTTON = 1;
	public View delete_fav_view;
	public TextView deleteTextTV;
	TextView tv22;
	TextView noFavouritesadded;
	
	public static String headerTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		// must assign these
		Utility.hotOffersScreen = "null";
		Utility.AroundMeScreen = "null";
		Utility.DailyDealsScreen ="null";
		Utility.ResultScreen ="null";
		registerBaseActivityReceiver();
		
		try {
			setUpViewsByHari();
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}
	
	private void setUpViewsByHari() {
		headerTitle = getResources().getString(R.string.my_favourites_text);
		setHeaderTitle(headerTitle);
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		loading = (View) findViewById(R.id.loading);
		loading.setVisibility(View.GONE);
		dbHelper = new DatabaseHelper(this);
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();

		favoritesListView = (ListView) findViewById(R.id.resultsListViewID);
		myFavoriteProductsList = new ArrayList<Product>();

		hotOffersProductsList = new ArrayList<Product>();
		favoritesListView.setOnItemClickListener(this);
		
		try {
			myFavoriteProductsList = dbHelper.getProductList();
			if (myFavoriteProductsList.size() == 0) {
				noFavouritesadded = (TextView) findViewById(R.id.noFavAdded_deleteTVID);
				noFavouritesadded.setTypeface(Utility.font_bold);
				noFavouritesadded.setVisibility(View.VISIBLE);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
		
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			if (Utility.user != null) {
				if (Utility.user.getClientBanner() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				}
			}
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
		}
		
		mAdapter = new MyFavoritesAdapter(this);
		favoritesListView.setAdapter(mAdapter);
		favoritesListView.setOnItemClickListener(this);
	}
	
	@Override
		protected void onResume() {
			super.onResume();
			try {
				setUpViewsByHari();
			} catch (NullPointerException e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
				}
			}
		}

	@Override
	protected void onDestroy() {
	   super.onDestroy();
	   System.gc();
	   unRegisterBaseActivityReceiver();
	}
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		//int loader = R.drawable.loading_imp;
		//int loader = banner.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = _bannerPath;
        Log.w("Hari-->", _bannerPath);
        
        // ImageLoader class instance
      //  BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        try {
        	banner.setImageUrl(image_url);
        //	imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	public class MyFavoritesAdapter extends BaseAdapter {

		public MyFavoritesAdapter(FavoritesListActivity favoritesListActivity) {

		}

		@Override
		public int getCount() {
			return myFavoriteProductsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(final int pos, View view, ViewGroup arg2) {
			try {
				View resultsListRow = null;
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(
							R.layout.my_favorites_list_item, null, false);
				}
				RelativeLayout rowLL = (RelativeLayout) resultsListRow
						.findViewById(R.id.favResultListItemLLID);
				RelativeLayout rowLL2 = (RelativeLayout) resultsListRow
						.findViewById(R.id.resultItemLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.0);
				TextView productNameTV = (TextView) resultsListRow
						.findViewById(R.id.productMyTVID);
				productNameTV.setTypeface(Utility.font_bold);
				TextView highlightTV = (TextView) resultsListRow
						.findViewById(R.id.offerMyTVID);
				highlightTV.setTypeface(Utility.font_reg);
				Button deleteBtn = (Button) resultsListRow
						.findViewById(R.id.deleteMyFavoriteBtnID);
				deleteBtn.setFocusable(false);
				deleteBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {

						if (v.getId() == R.id.deleteMyFavoriteBtnID) {
							Utility.fav_position_var = pos;
							showDialog(DELETE_MY_FAVORITE_BUTTON);
						} else {
							if (menuListView.getVisibility() == ListView.GONE) {
								if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
									if (menuListView.getVisibility() == ListView.GONE) {
										Intent detailsIntent = new Intent(FavoritesListActivity.this,ProductDetailsActivity.class);
										detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,	hotOffersProductsList.get(pos).getId());
										detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,	pos % 4);
										detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY,hotOffersProductsList.get(pos).getName());
										detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY, hotOffersProductsList.get(pos).getHighlight());
										detailsIntent.putExtra(Utility.FINISHED_STATUS, "FAVOURITES_SCREEN");
										startActivity(detailsIntent);
										//FavoritesListActivity.this.finish();
									}
								} else {
									// The Custom Toast Layout Imported here
									LayoutInflater inflater = getLayoutInflater();
									View layout = inflater
											.inflate(
													R.layout.toast_no_netowrk,
													(ViewGroup) findViewById(R.id.custom_toast_layout_id));

									// The actual toast generated here.
									Toast toast = new Toast(getApplicationContext());
									toast.setDuration(Toast.LENGTH_LONG);
									toast.setView(layout);
									toast.show();
								}
							}
						}
					}
				});
				productNameTV.setText(myFavoriteProductsList.get(pos).getName());
				highlightTV.setText(myFavoriteProductsList.get(pos).getHighlight());
				
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				}
				return resultsListRow;
			
			} catch (Exception e) {
				if (e != null) {
					Log.w("HARI-->DEBUG", e);
				}
				return null;
			}
		}
	}

	// --------------Hari-------------------- //

	// Hari----------------------
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog deleteFavAlert = null;
			LayoutInflater liDelete = LayoutInflater.from(this);
			View deleteFavView = liDelete.inflate(R.layout.dialog_layout_delete_favorite, null);
			AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(this);
			adbDeleteFav.setView(deleteFavView);
			deleteFavAlert = adbDeleteFav.create();
			deleteFavAlert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
			deleteFavAlert.show();
			return deleteFavAlert;
		} else if (id == LOGOUT) {
			AlertDialog dialogDetails = null;
			LayoutInflater inflater = LayoutInflater.from(this);
			View dialogview = inflater.inflate(R.layout.dialog_layout_logout, null);
			AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
			dialogbuilder.setView(dialogview);
			dialogDetails = dialogbuilder.create();
			dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
			dialogDetails.show();
			return dialogDetails;
		} else if (id == MY_FAVOURITES) {
			AlertDialog noFavalert = null;
			LayoutInflater in1 = LayoutInflater.from(this);
			View dview = in1.inflate(R.layout.dialog_layout_fav_no, null);
			AlertDialog.Builder dbl1 = new AlertDialog.Builder(this);
			dbl1.setView(dview);
			noFavalert = dbl1.create();
			noFavalert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
			noFavalert.show();
			return noFavalert;
		}
		return null;
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
				final AlertDialog alt3 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alt3
						.findViewById(R.id.favGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				tv22 = (TextView) alt3.findViewById(R.id.deleteFavTVID);
				tv22.setTypeface(Utility.font_reg);

				tv22.setText(getResources().getString(
						R.string.remove_favourite_message1)
						+ " "
						+ myFavoriteProductsList.get(Utility.fav_position_var)
								.getName()
						+ " "
						+ getResources().getString(
								R.string.remove_favourite_message2)+"?");
				Button deleteFavYesBtn = (Button) alt3
						.findViewById(R.id.delete_fav_yesBtnID);
				deleteFavYesBtn.setTypeface(Utility.font_bold);
				Button deleteNoFavBtn = (Button) alt3
						.findViewById(R.id.delete_fav_noBtnID);
				deleteNoFavBtn.setTypeface(Utility.font_bold);
				deleteFavYesBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Log.v("Hi", "This is Hari from Favourite");
						dbHelper.deleteProduct(Integer
								.toString(myFavoriteProductsList.get(
										Utility.fav_position_var).getId()));
						myFavoriteProductsList = dbHelper.getProductList();
						mAdapter.notifyDataSetChanged();
						if (myFavoriteProductsList.size() == 0) {
							noFavouritesadded = (TextView) findViewById(R.id.noFavAdded_deleteTVID);
							noFavouritesadded.setVisibility(View.VISIBLE);
						}
						alt3.dismiss();
					}
				});
				deleteNoFavBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						alt3.dismiss();
					}
				});
		} else if (id == LOGOUT) {

			final AlertDialog alertDialog1 = (AlertDialog) dialog;

			TextView alertTitle = (TextView) alertDialog1
					.findViewById(R.id.firstLoginGINTitleTVID);
			alertTitle.setTypeface(Utility.font_bold);

			logoutText = (TextView) alertDialog1
					.findViewById(R.id.my_logoutTVID);
			logoutText.setTypeface(Utility.font_reg);
			// logoutText.setVisibility(View.VISIBLE);
			loginbutton1 = (Button) alertDialog1
					.findViewById(R.id.loginBtnH_ID);
			loginbutton1.setTypeface(Utility.font_bold);
			cancelbutton1 = (Button) alertDialog1
					.findViewById(R.id.btn_cancelH_ID);
			cancelbutton1.setTypeface(Utility.font_bold);

			loginbutton1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						if (helper != null) {
							helper.deleteLoginDetails();
							closeAllActivitiesByHari();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("Hari-->Debug", e);
						}
					}
					Intent logoutIntent = new Intent(FavoritesListActivity.this, LoginActivity.class);
					logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					logoutIntent.putExtra("finish", true);
					startActivity(logoutIntent);
					LoginActivity.etUserId.setText("");
					LoginActivity.etPasswd.setText("");
					LoginActivity.subDomainURL_et.setText("");
					
					FavoritesListActivity.this.finish();
					alertDialog1.dismiss();
				}
			});
			cancelbutton1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.dismiss();
				}
			});
		}

		else if (id == MY_FAVOURITES) {
			final AlertDialog noFavalert = (AlertDialog) dialog;
			TextView alertTitle = (TextView) noFavalert.findViewById(R.id.firstLoginGINTitleTVID);
			alertTitle.setTypeface(Utility.font_bold);
			noFavTV = (TextView) noFavalert.findViewById(R.id.nofavTVID);
			noFavTV.setTypeface(Utility.font_reg);
			noFavOKBtn = (Button) noFavalert.findViewById(R.id.noFavOkBtnID);
			noFavOKBtn.setTypeface(Utility.font_bold);
			noFavOKBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					noFavalert.dismiss();
				}
			});
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos, long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
				if (menuListView.getVisibility() == ListView.GONE) {
					Intent detailsIntent = new Intent(FavoritesListActivity.this, ProductDetailsActivity.class);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,	myFavoriteProductsList.get(pos).getId());
					detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY, pos % 4);
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_NAME_KEY, myFavoriteProductsList.get(pos).getName());
					detailsIntent.putExtra(ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,	myFavoriteProductsList.get(pos).getHighlight());
					startActivity(detailsIntent);
				}
			} else {
				// The Custom Toast Layout Imported here -->by HARI
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toast_no_netowrk,
				(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
				// The actual toast generated here.
				Toast toast = new Toast(getApplicationContext());
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(FavoritesListActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			headerTitle = null;
			FavoritesListActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
}
