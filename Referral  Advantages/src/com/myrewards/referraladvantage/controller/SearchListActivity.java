package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.Category;
import com.myrewards.referraladvantage.model.NoticeId;
import com.myrewards.referraladvantage.model.User;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

//import com.myrewards.grabitnow.model.NoticeId;

public class SearchListActivity extends HomeBasedActivity implements
		OnItemClickListener, OnClickListener, ServiceListener,
		OnLongClickListener  { //IImageURLTaskListener
	LayoutInflater inflater;
	SearchListAdapter mAdapter;
	ArrayList<Category> categoryList;
	ArrayList<Category> categoryListTemp;
	List<NoticeId> noticeid;
	View loading;
	Button searchBtn;
	ListView searchListView;
	Button resetBtn;
	EditText locationET;
	EditText keywordET;
//	DatabaseHelper helper;
	ImageView my_info_btn1;
	CheckBox newcheckbox;
	TextView textTv;
	private static final int SEARCH_LIST_SELECTION = 1;

	// this is for login activity variables
	DatabaseHelper dbHelper;

	final private static int NO_NETWORK_CON = 4;

	//ImageView bannerIV;
	
	Bundle bundle = null;
	String loginIntent = null;
	boolean finishState;
	boolean doubleBackToExitPressedOnce = false;
	
	public static String headerTitle = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_list_main);
		registerBaseActivityReceiver();
		setDimensions();
		dbHelper = new DatabaseHelper(this);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		helper = new DatabaseHelper(this);
		headerTitle = getResources().getString(R.string.search_text);
		setHeaderTitle(headerTitle);

		categoryList = new ArrayList<Category>();
		categoryListTemp = new ArrayList<Category>();
		
		loading = (View) findViewById(R.id.loading);
		Button helpBtn;
		helpBtn = (Button) findViewById(R.id.helpBtnID);
		helpBtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		helpBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		helpBtn.setOnClickListener(this);
		locationET = (EditText) findViewById(R.id.enterLocationETID);

		locationET.setTypeface(Utility.font_reg);

		locationET.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		keywordET = (EditText) findViewById(R.id.enterKeywordETID);

		keywordET.setTypeface(Utility.font_reg);

		keywordET.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		searchBtn = (Button) findViewById(R.id.searchBtnID);
		searchBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		searchBtn.setOnClickListener(this);
		resetBtn = (Button) findViewById(R.id.resetBtnID);
		resetBtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		resetBtn.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
		resetBtn.setOnClickListener(this);
		searchListView = (ListView) findViewById(R.id.searchListViewID);
		searchListView.setOnItemClickListener(this);

		locationET.setOnLongClickListener(this);
		keywordET.setOnLongClickListener(this);

		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();

		if (getIntent() != null) {
			try {
				bundle = getIntent().getExtras();
				if (bundle != null) {
					loginIntent = bundle.getString("LOGIN_ACTIVITY");
					// Hari---- > finish() all activities 
					finishState = bundle.getBoolean("finish", false);
				}
				if (loginIntent != null || bundle == null) {
					if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
						if (Utility.user != null) {
							GrabItNowService.getGrabItNowService().sendNoticeBoardCountIdRequset(this);
							GrabItNowService.getGrabItNowService().sendCategoriesListRequest(this);
						}
					} else {
						showDialog(NO_NETWORK_CON);
					}
				} else if (finishState) {
					Intent i = new Intent(this, SearchListActivity.class);//here code stopped
					i.putExtra("LOGIN_ACTIVITY", "LOGIN_ACTIVITY");
					startActivity(i);
					SearchListActivity.this.finish();
					return;
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			System.gc();
			Runtime.getRuntime().gc();
			unRegisterBaseActivityReceiver();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}

	public class SearchListAdapter extends BaseAdapter {

		public SearchListAdapter(SearchListActivity searchListActivity) {

		}

		@Override
		public int getCount() {
			return categoryList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View searchListRow = null;
			try {
				if (searchListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					searchListRow = (ViewGroup) inflater.inflate(
							R.layout.search_list_item, null, false);
				}
				LinearLayout searchlistLL = (LinearLayout) searchListRow
						.findViewById(R.id.searchListsLLID);
				searchlistLL.getLayoutParams().height = (int) (Utility.screenHeight / 14.0);
				TextView category = (TextView) searchListRow
						.findViewById(R.id.categoryTVID);
				ImageView catImage = (ImageView) searchListRow
						.findViewById(R.id.catImageID);

				category.setText(categoryList.get(pos).getName());
				category.setTypeface(Utility.font_bold);
				category.setTextColor(getResources().getColor(R.color.handlife));
				
				String cat=categoryList.get(pos).getName();
				if(cat.toLowerCase().contains("automotive"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type1);
				}
				
				else if(cat.toLowerCase().contains("dining"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type2);
				}
				else if(cat.toLowerCase().contains("golf courses"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type8);
				}
				else if(cat.toLowerCase().contains("golf handicaps") || cat.toLowerCase().contains("handicaps"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type8);
				}
				else if(cat.toLowerCase().contains("golf insurance") || cat.toLowerCase().contains("insurance"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type9);
				}
				else if(cat.toLowerCase().contains("health"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type3);
				}
				else if(cat.toLowerCase().contains("home"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type4);
				}
				else if(cat.toLowerCase().contains("lifestyle"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type4);
				}
				else if(cat.toLowerCase().contains("entertainment") || cat.toLowerCase().contains("leisure"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type5);
				}
				else if(cat.toLowerCase().contains("shop"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type6);
				}
				else if(cat.toLowerCase().contains("travel"))
				{
					catImage.setBackgroundResource(R.drawable.pin_type7);
				}
				else 
				{
					catImage.setBackgroundResource(R.drawable.pin_type10);
				}

				RadioButton selectedRowRB = (RadioButton) searchListRow.findViewById(R.id.categoryRBID);
				if (categoryList.get(pos).isSelected())
					selectedRowRB.setChecked(true);
				else
					selectedRowRB.setChecked(false);
			
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
				return null;
			}
			
			return searchListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.searchListViewID) {
			try {
				if (menuListView.getVisibility() == ListView.GONE) {
					for (int i = 0; i < categoryList.size(); i++) {
						categoryList.get(i).setSelected(false);
					}
					categoryList.get(pos).setSelected(true);
					mAdapter.notifyDataSetChanged();
				}
			} catch (Exception e) {
				Log.w("HARI-->DEUBUG", e);
			}
		}
	}

	@Override
	public void onClick(View view) {
		super.onClick(view);
			switch (view.getId()) {
			case R.id.helpBtnID:
				try {
					LayoutInflater inflater = getLayoutInflater();
					View helpDialogView = (View) inflater.inflate(R.layout.help_dialog,	null, false);
					final Dialog messageDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
					TextView windowTitle = (TextView) helpDialogView.findViewById(R.id.info_heading);
					windowTitle.setTypeface(Utility.font_reg);

					TextView helpDesc1TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc1TVID);
					helpDesc1TVID.setTypeface(Utility.font_reg);

					TextView helpDesc2TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc2TVID);
					helpDesc2TVID.setTypeface(Utility.font_reg);

					TextView helpDesc8TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc8TVID);
					helpDesc8TVID.setTypeface(Utility.font_reg);

					TextView helpDesc3TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc3TVID);
					helpDesc3TVID.setTypeface(Utility.font_reg);

					TextView helpDesc4TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc4TVID);
					helpDesc4TVID.setTypeface(Utility.font_reg);

					TextView helpDesc5TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc5TVID);
					helpDesc5TVID.setTypeface(Utility.font_reg);

					TextView helpDesc6TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc6TVID);
					helpDesc6TVID.setTypeface(Utility.font_reg);

					TextView helpDesc7TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc7TVID);
					helpDesc7TVID.setTypeface(Utility.font_reg);

					TextView helpDesc9TVID = (TextView) helpDialogView.findViewById(R.id.helpDesc9TVID);
					
					helpDesc9TVID.setText(getResources().getString(R.string.info_text9)+"\n \n"+getResources().getString(R.string.info_text10)+"\n \n"+getResources().getString(R.string.info_text11));
					
					helpDesc9TVID.setTypeface(Utility.font_reg);

					Window window = messageDialog.getWindow();
					window.setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
					window.setLayout((int) (8 * Utility.screenWidth / 9.2),	5 * Utility.screenHeight / 6);
					window.getAttributes().dimAmount = 0.7f;
					messageDialog.setCancelable(true);
					messageDialog.setContentView(helpDialogView);
					messageDialog.show();
					ImageView myhee = (ImageView) helpDialogView.findViewById(R.id.infoButtonIVID);
					// ImageView myhee=(ImageView)findViewById(R.id.infoButtonIVID);
					myhee.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							messageDialog.cancel();
						}
					});
				} catch (Exception e) {
					if (e != null) {
						Log.w("Hari--> DEBUG", e);
					}
				}
				
				break;
			case R.id.searchBtnID:
				String location = locationET.getText().toString();
				String keyword = keywordET.getText().toString();
				String catID = null;
				for (int i = 0; i < categoryList.size(); i++) {
					if (categoryList.get(i).isSelected()) {
						catID = Integer.toString(categoryList.get(i).getCatID());
						break;
					}
				}
				if (catID == null) {
					showDialog(SEARCH_LIST_SELECTION);
				} else {
					try {
						if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
							Intent resultIntent = new Intent(SearchListActivity.this, ResultsListActivity.class);
							resultIntent.putExtra(ApplicationConstants.CAT_ID_KEY, catID);
							resultIntent.putExtra(ApplicationConstants.LOCATION_KEY, location.trim());
							resultIntent.putExtra(ApplicationConstants.KEYWORK_KEY, keyword.trim());
							startActivity(resultIntent);
							//SearchListActivity.this.finish();
						} else {
							// The Custom Toast Layout Imported here
							LayoutInflater inflater = getLayoutInflater();
							View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));
										 
							// The actual toast generated here.
							Toast toast = new Toast(getApplicationContext());
							toast.setDuration(Toast.LENGTH_LONG);
							toast.setView(layout);
							toast.show();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("Hari--DEBUG", e);
						}
					}
				}
				break;
			case R.id.resetBtnID:
				try {
					keywordET.setText("");
					locationET.setText("");
					for (int i = 0; i < categoryList.size(); i++) {
						categoryList.get(i).setSelected(false);
					}
					mAdapter.notifyDataSetChanged();
				} catch (Exception e) {
					if (e != null) {
						Log.w("Hari--DEBUG", e);
					}
				}
				break;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try{
		if (eventType == 16) {
			if (response != null) {
				noticeid = (ArrayList<NoticeId>) response;
				int i = -1;
				if (noticeid != null)
				{
					List<String> noticesCountList=new ArrayList<String>();
					for(int n=0;n<noticeid.size();n++)
					{
						if(!(noticeid.get(n).getNoticeDetails()==null))
							noticesCountList.add(noticeid.get(n).getNoticeDetails());
					}
						if(noticesCountList.size()!=dbHelper.getExistingIDs().size())
						{
							for(int m=0;m<dbHelper.getExistingIDs().size();m++)
							{
								if(noticesCountList.contains(dbHelper.getExistingNoticeRealIDs().get(m)))
								{
									
								}
								else
								{
									dbHelper.deleteNoticeIdDetails(dbHelper.getExistingNoticeRealIDs().get(m));
								}
							}
						}
					
					for (int k = 0; k < noticeid.size(); k++) {
						if (noticeid.get(k).getNoticeDetails() != null) {
							i++;
							List<String> list = dbHelper.getExistingIDs();
							if (!list.contains(noticeid.get(k).getNoticeDetails())) {
								dbHelper.addnoticeiddetails(noticeid.get(k)
										.getNoticeDetails());
							}
						}
					}
					Log.v("Hari---->", "Count = "+i);				
				}
			}
		}

		else if (eventType == 2 && response instanceof User) {
			if (response != null) {
				if (response instanceof String) {
				} else {
					dbHelper.addLoginDetails(Utility.user_Name,
							Utility.user_Password, Utility.user_website,
							dbHelper.getLoginDetails().getLoginDate());
					Utility.user = (User) response;
					// Utility.user=Translate.execute("How are you",
					// Language.ENGLISH, Language.FRENCH);
					categoryList = new ArrayList<Category>();
					categoryListTemp = new ArrayList<Category>();
					if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (Utility.user != null) {
							GrabItNowService.getGrabItNowService().sendNoticeBoardCountIdRequset(this);
							GrabItNowService.getGrabItNowService().sendCategoriesListRequest(this);
						}
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_no_netowrk,
						(ViewGroup) findViewById(R.id.custom_toast_layout_id));
									 
						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
					}
				}
			}
		}

		else {
			if(eventType!=16)
			{
			if (response != null) {
				if (response instanceof String) {
					// showErrorDialog(response.toString());
				//	Utility.showMessage(this, response.toString());
					loading.setVisibility(View.GONE);
				} else {
					if(eventType==4)
					{
						if (Utility.user != null) {
							if (Utility.user.getClientBanner() == null) {
								String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
								newImagesLoading(bannerURL);
							} else {
								//onImageLoadComplete(Utility.user.getClientBanner());
							}
						}
					categoryList.clear();
					categoryList.addAll((Collection<Category>) response);
					if (categoryListTemp != null) {
						int position = categoryListTemp.size();
						categoryListTemp.addAll(categoryList);
						categoryList = categoryListTemp;
						searchListView.setSelection(position);
					}
					mAdapter = new SearchListAdapter(this);
					searchListView.setAdapter(mAdapter);
					loading.setVisibility(View.GONE);

					menuListView.setVisibility(ListView.VISIBLE);
					if(getIntent()!=null)
					{
						if(getIntent().getStringExtra("LOGIN_ACTIVITY")!=null)
						{
							applyMenuListSlideAnimation(-1, 0);
						}
						else
						{
							applyMenuListSlideAnimation(0, -1);
							menuListView.setVisibility(ListView.GONE);
						}
					}
					else
					{
						applyMenuListSlideAnimation(-1, 0);
					}
					
					try {
						//this is for notifying the user regarding the updates of notice boards
						if(dbHelper.getNoticeUpdateState().equals("NO_VALUE"))
						{
						dbHelper.setNoticeUpdateState("ON");
						Utility.setNotificationReceiver(this);
						}
					} catch (Exception e) {
						if (e != null) {
							e.printStackTrace();
							Log.w("HARI-->DEBUG", e);
						}
					}
				}
			}
			}
		}
		}
		}catch(Exception e)
		{
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}		
		}
	}
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		//bannerIV.getLayoutParams().width = Utility.screenWidth * 1;
		
		//int loader = R.drawable.loading_imp;
		//int loader = bannerIV.getWidth();
		//Log.w("Hari-->", String.valueOf(loader));
		//int loader = bannerIV.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = _bannerPath;
        Log.w("Hari-->", _bannerPath);
        
        // ImageLoader class instance
    //    BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        //imgLoader.DisplayImage(image_url, loader, bannerIV);
        try {
        	banner.setImageUrl(image_url);
        	// imgLoader.DisplayImage(image_url, bannerIV);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	protected void onRestart() {
		menuListView.setVisibility(ListView.GONE);
		super.onRestart();
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog dialogDetails = null;
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(
						R.layout.dialog_layout_search_list_location, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				return dialogDetails;
			}
			else if (id == LOGOUT) {
				AlertDialog dialogDetails = null;

				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(R.layout.dialog_layout_logout,
						null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				return dialogDetails;
			} else if (id == MY_FAVOURITES) {
				AlertDialog noFavalert = null;

				LayoutInflater in1 = LayoutInflater.from(this);
				View dview = in1.inflate(R.layout.dialog_layout_fav_no, null);
				AlertDialog.Builder dbl1 = new AlertDialog.Builder(this);
				dbl1.setView(dview);
				noFavalert = dbl1.create();
				noFavalert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noFavalert.show();
				return noFavalert;
			}
			else if (id == 4) {
				AlertDialog noNetworkDialog = null;
				LayoutInflater noNetInflater = LayoutInflater.from(this);
				View noNetworkView = noNetInflater.inflate(
						R.layout.dialog_layout_no_network, null);
				AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
				adbNoNet.setView(noNetworkView);
				adbNoNet.setCancelable(false);
				noNetworkDialog = adbNoNet.create();
				noNetworkDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noNetworkDialog.show();
				return noNetworkDialog;
			}		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
			return null;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;

				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				TextView alertMessage = (TextView) alertDialog2
						.findViewById(R.id.searchInvalidTVID);
				alertMessage.setTypeface(Utility.font_reg);

				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.inValidOKBtnID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
					}
				});
			} else if (id == LOGOUT) {

				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				logoutText = (TextView)alertDialog1.findViewById(R.id.my_logoutTVID);
			//	logoutText.setText("Do you want to exit the application?");
				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.firstLoginGINTitleTVID);
				
				alertTitle.setTypeface(Utility.font_bold);
			//	alertTitle.setText("Exit !");

				logoutText.setTypeface(Utility.font_reg);
				// logoutText.setVisibility(View.VISIBLE);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);

				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							if (helper != null) {
								helper.deleteLoginDetails();
								closeAllActivitiesByHari();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(SearchListActivity.this,
								LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						SearchListActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
			} else if (id == 3) {
				final AlertDialog noFavalert = (AlertDialog) dialog;
				TextView alertTitle = (TextView) noFavalert.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				noFavTV = (TextView) noFavalert.findViewById(R.id.nofavTVID);
				noFavTV.setTypeface(Utility.font_reg);
				noFavOKBtn = (Button) noFavalert.findViewById(R.id.noFavOkBtnID);
				noFavOKBtn.setTypeface(Utility.font_bold);

				noFavOKBtn.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						noFavalert.dismiss();
					}
				});
			}

			else if (id == 4) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				textTv = (TextView) alertDialog2.findViewById(R.id.noConnTVID);
				
				textTv.setTypeface(Utility.font_reg);
				
				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.alertLogoutTitleTVID);
				
				alertTitle.setTypeface(Utility.font_bold);
				
				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.noNetWorkOKID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						try {
							loading.setVisibility(View.GONE);
							if (menuListView.getVisibility() == View.GONE) {
								menuListView.setVisibility(View.VISIBLE);
								applyMenuListSlideAnimation(-1, 0);
							} else {
								applyMenuListSlideAnimation(0, -1);
								menuListView.setVisibility(View.GONE);
							}
							alertDialog2.dismiss();
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("HARI-->DEBUG", e);
							}
						}
					}
				});
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void setDimensions() {
		try {
			Display display = getWindowManager().getDefaultDisplay();
			int screenWidth = display.getWidth();
			int screenHeight = display.getHeight();
			Utility.screenWidth = screenWidth;
			Utility.screenHeight = screenHeight;
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if (keyCode==KeyEvent.KEYCODE_BACK) {
			if (doubleBackToExitPressedOnce) {
					closeAllActivitiesByHari();
					SearchListActivity.this.finish();
					return doubleBackToExitPressedOnce;
				}
				doubleBackToExitPressedOnce = true;
				if (menuListView.getVisibility() == View.VISIBLE) {
					applyMenuListSlideAnimation(0, -1);
					menuListView.setVisibility(View.GONE);
				}
				Toast.makeText(SearchListActivity.this, "Press again to minimize the app.", 100).show();
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						doubleBackToExitPressedOnce = false;
					}
				}, 2000);
				return true;
	  }
	     return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = false;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);

			return returnValue;
		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
		return returnValue;
	}
}
