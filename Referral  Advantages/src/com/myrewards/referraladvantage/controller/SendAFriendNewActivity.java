package com.myrewards.referraladvantage.controller;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

public class SendAFriendNewActivity extends HomeBasedActivity implements ServiceListener, OnLongClickListener {
	Button sendBtn11;
	Button sendBtn;
	LayoutInflater inflater;
	EditText mUname, mEmail;
	DatabaseHelper helper;
	ListView emailList;
	private static final int DIALOG_SEND = 99;
	private static final int DIALOG_SEND_SUCCESS = 98;
	private static final int DIALOG_SEND_FAILED = 97;
	private static final int DIALOG_SEND_FIELDS_ERROR = 96;
	private static final int DIALOG_SEND_INVALID_EMAIL = 95;
	//User user;
	TextView textTV, titleTV;
	TextView alertTilteTv, alertMsgTV;
	Button okbutton;
	public static String headerTitle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_a_friend);
		registerBaseActivityReceiver();
		
		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);
		
		menuBtn = (Button) findViewById(R.id.menuBtnID);
		menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		
		menuListView = (ListView) findViewById(R.id.menuListViewID);
		initialiseViews();
		headerTitle = getResources().getString(R.string.send_a_friend);
		setHeaderTitle(headerTitle);
		
		helper = new DatabaseHelper(this);
		
		if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
			if (Utility.user != null) {
				if (Utility.user.getClientBanner() == null) {
					String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
					newImagesLoading(bannerURL);
				} else {
					//onImageLoadComplete(Utility.user.getClientBanner());
				}				
			} 
		} else {
			// The Custom Toast Layout Imported here
			LayoutInflater inflater = getLayoutInflater();
			View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));

			// The actual toast generated here.
			Toast toast = new Toast(getApplicationContext());
			toast.setDuration(Toast.LENGTH_LONG);
			toast.setView(layout);
			toast.show();
			SendAFriendNewActivity.this.finish();
		}
		
		sendBtn11 = (Button) findViewById(R.id.sendafrndbtnID);
		sendBtn11.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showDialog(DIALOG_SEND);
			}
		});
	}
	
	private void newImagesLoading(String _bannerPath) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		banner.getLayoutParams().width = Utility.screenWidth;
		
		//int loader = R.drawable.loading_imp;
		//int loader = banner.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = _bannerPath;
        Log.w("Hari-->", _bannerPath);
        
        // ImageLoader class instance
     //   BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        try {
        	banner.setImageUrl(image_url);
        	// imgLoader.DisplayImage(image_url, banner);
		} catch (OutOfMemoryError e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (response != null) {
				if (eventType == 17) {
					if (response.toString().contains("SUCCESS")	|| response.toString().contains("success")) {
						showDialog(DIALOG_SEND_SUCCESS);
					} else {
						showDialog(DIALOG_SEND_FAILED);
					}
				}
			}
		
		} catch (Exception e) {
			if (e != null) {
				Log.v("Hari--> SendAFriend Onservicecomplete", e.getMessage().toString());
				Toast.makeText(getApplicationContext(), "Server busy. Please wait or try again.", Toast.LENGTH_LONG).show();
				e.printStackTrace();
			}
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
	
		if (id == 99) {
			AlertDialog sendDetails = null;
			switch (id) {
			case DIALOG_SEND:
				LayoutInflater inflateSend = LayoutInflater.from(this);
				View deleteFavView = inflateSend.inflate(
						R.layout.dialog_layout_send_emails_frnds, null);
				AlertDialog.Builder adbDeleteFav = new AlertDialog.Builder(this);
				adbDeleteFav.setView(deleteFavView);
				sendDetails = adbDeleteFav.create();
				break;
			}
			return sendDetails;
		} else if (id == 98) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case DIALOG_SEND_SUCCESS:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(R.layout.dialog_send_a_frnd_success, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				break;
			}
			return dialogDetails2;
		} else if (id == 97) {
			AlertDialog dialogDetails2 = null;
			switch (id) {
			case DIALOG_SEND_FAILED:
				LayoutInflater inflater2 = LayoutInflater.from(this);
				View dialogview = inflater2.inflate(R.layout.dialog_send_a_frnd_failed, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails2 = dialogbuilder.create();
				break;
			}
			return dialogDetails2;
		} else if (id == 96) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_FIELDS_ERROR:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(R.layout.dialog_layout_first_login_fields_error, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				break;
			}
			return fieldsDialog;
		}
		else if (id == 95) {
			AlertDialog fieldsDialog = null;
			switch (id) {
			case DIALOG_SEND_INVALID_EMAIL:
				LayoutInflater mli = LayoutInflater.from(this);
				View allFieldsView = mli.inflate(R.layout.dialog_send_a_frnd_invalid, null);
				AlertDialog.Builder allFalert = new AlertDialog.Builder(this);
				allFalert.setView(allFieldsView);
				fieldsDialog = allFalert.create();
				break;
			}
			
			return fieldsDialog;
		}
		else if (id == LOGOUT) {
			AlertDialog dialogDetails = null;
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(
						R.layout.dialog_layout_logout, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
			return dialogDetails;
		} else if (id == MY_FAVOURITES) {
			AlertDialog noFavalert = null;
				LayoutInflater in1 = LayoutInflater.from(this);
				View dview = in1.inflate(R.layout.dialog_layout_fav_no, null);
				AlertDialog.Builder dbl1 = new AlertDialog.Builder(this);
				dbl1.setView(dview);
				noFavalert = dbl1.create();
				noFavalert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noFavalert.show();
			return noFavalert;
		}
		return null;
	}

	@SuppressLint("ShowToast")
	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
	
		if (id == 99) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
				alertTilteTv = (TextView)alertDialogCustom.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
				alertTilteTv.setTypeface(Utility.font_bold);
				mUname = (EditText) alertDialogCustom.findViewById(R.id.nameId);
				mUname.setTypeface(Utility.font_reg);
				mUname.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);
				mEmail = (EditText) alertDialogCustom.findViewById(R.id.emailId);
				mEmail.setTypeface(Utility.font_reg);
				mEmail.getLayoutParams().height = (int) (Utility.screenHeight / 16.0);

				mUname.setOnLongClickListener(this);
				mEmail.setOnLongClickListener(this);
				
				final InputMethodManager im = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
				mEmail.setOnClickListener(new OnClickListener() {

			        @Override
			        public void onClick(View v) {
			            // TODO Auto-generated method stub
			        im.showSoftInput(mEmail, InputMethodManager.SHOW_IMPLICIT);
			        }
			    });
				
				final InputMethodManager im2 = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
				mUname.setOnClickListener(new OnClickListener() {

			        @Override
			        public void onClick(View v) {
			            // TODO Auto-generated method stub
			        im2.showSoftInput(mUname, InputMethodManager.SHOW_IMPLICIT);
			        }
			    });

				Button sendBtn = (Button) alertDialogCustom.findViewById(R.id.sendBtnID);
				sendBtn.setTypeface(Utility.font_bold);
				alertDialogCustom.setCancelable(false);
				Button sendCloseBtn = (Button) alertDialogCustom
						.findViewById(R.id.sendCloseBtnID);
				sendCloseBtn.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
						im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
						alertDialogCustom.dismiss();
					}
				});
				sendBtn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						String frndName = mUname.getText().toString();
						final String frndEmailID = mEmail.getText().toString();
						if (frndName != null && frndName.trim().length() > 0) {
							if (isEmailValid(frndEmailID)) {
								if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
									GrabItNowService.getGrabItNowService()
									.sendSendAFriendRequest(SendAFriendNewActivity.this,
											Utility.user.getId(),
											mUname.getText().toString(),
											mEmail.getText().toString());
								} else {
									alertDialogCustom.dismiss();
									// The Custom Toast Layout Imported here
									LayoutInflater inflater = getLayoutInflater();
									View layout = inflater.inflate(R.layout.toast_no_netowrk,
									(ViewGroup) findViewById(R.id.custom_toast_layout_id));
									//layout.getBackground().setAlpha(128);  // 50% transparent
									 
									// The actual toast generated here.
									Toast toast = new Toast(getApplicationContext());
									toast.setDuration(Toast.LENGTH_LONG);
									toast.setView(layout);
									toast.show();
								}
							} else {
								showDialog(DIALOG_SEND_INVALID_EMAIL);
							}

						} else {
							showDialog(DIALOG_SEND_FIELDS_ERROR);
						}
						im.hideSoftInputFromWindow(mEmail.getWindowToken(), 0);
						im2.hideSoftInputFromWindow(mUname.getWindowToken(), 0);
					}
				});
		} else if (id == 98) {
			final AlertDialog alertDialogCustoms= (AlertDialog) dialog;
			alertTilteTv = (TextView)alertDialogCustoms.findViewById(R.id.firstLoginCEPUTitleTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustoms.findViewById(R.id.succSentTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustoms.findViewById(R.id.sendSuccOkBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialogCustoms.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent i = new Intent(SendAFriendNewActivity.this, SearchListActivity.class);
					i.putExtra("finish", true);
					i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
					startActivity(i);
					headerTitle = null;
					SendAFriendNewActivity.this.finish();
					alertDialogCustoms.dismiss();
				}
			});
		} else if (id == 97) {
			final AlertDialog alertDialogCustoms = (AlertDialog) dialog;
			alertTilteTv = (TextView)alertDialogCustoms.findViewById(R.id.firstLoginCEPUimgTitleIVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustoms.findViewById(R.id.failedSentTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustoms.findViewById(R.id.sendFailedOkBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialogCustoms.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SendAFriendNewActivity.this.finish();
					alertDialogCustoms.dismiss();
				}
			});
		} else if (id == 96) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView)alertDialogCustom.findViewById(R.id.allFieldsTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustom.findViewById(R.id.allFieldsTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustom.findViewById(R.id.allFieldsOKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialogCustom.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialogCustom.dismiss();
				}
			});
		} else if (id == 95) {
			final AlertDialog alertDialogCustom = (AlertDialog) dialog;
			alertTilteTv = (TextView)alertDialogCustom.findViewById(R.id.firstLoginCEPUErrorTitleTVID);
			alertTilteTv.setTypeface(Utility.font_bold);
			alertMsgTV = (TextView) alertDialogCustom.findViewById(R.id.inValidFieldTVID);
			alertMsgTV.setTypeface(Utility.font_reg);
			okbutton = (Button) alertDialogCustom.findViewById(R.id.inValidOKBtnID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialogCustom.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialogCustom.dismiss();
				}
			});
		}
		else if (id == LOGOUT) {
			final AlertDialog alertDialog1 = (AlertDialog) dialog;
			logoutText = (TextView) alertDialog1
					.findViewById(R.id.my_logoutTVID);
			logoutText.setTypeface(Utility.font_bold);
			TextView alertTitle = (TextView) alertDialog1
					.findViewById(R.id.firstLoginGINTitleTVID);
			alertTitle.setTypeface(Utility.font_reg);
			// logoutText.setVisibility(View.VISIBLE);
			loginbutton1 = (Button) alertDialog1
					.findViewById(R.id.loginBtnH_ID);
			loginbutton1.setTypeface(Utility.font_bold);
			cancelbutton1 = (Button) alertDialog1
					.findViewById(R.id.btn_cancelH_ID);
			cancelbutton1.setTypeface(Utility.font_bold);

			loginbutton1.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						if (helper != null) {
							helper.deleteLoginDetails();
							closeAllActivitiesByHari();
						}
					} catch (Exception e) {
						if (e != null) {
							Log.w("Hari-->Debug", e);
						}
					}
					Intent logoutIntent = new Intent(
							SendAFriendNewActivity.this,
							LoginActivity.class);
					logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					// logoutIntent.putExtra("finish", true);
					startActivity(logoutIntent);
					LoginActivity.etUserId.setText("");
					LoginActivity.etPasswd.setText("");
					LoginActivity.subDomainURL_et.setText("");
					SendAFriendNewActivity.this.finish();
					alertDialog1.dismiss();
				}
			});
			cancelbutton1.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					alertDialog1.dismiss();
				}
			});
	} 
		else if (id == MY_FAVOURITES) {
			final AlertDialog noFavalert = (AlertDialog) dialog;
			TextView alertTitle = (TextView) noFavalert
					.findViewById(R.id.firstLoginGINTitleTVID);
			alertTitle.setTypeface(Utility.font_bold);

			noFavTV = (TextView) noFavalert.findViewById(R.id.nofavTVID);
			noFavTV.setTypeface(Utility.font_reg);

			noFavOKBtn = (Button) noFavalert
					.findViewById(R.id.noFavOkBtnID);
			noFavOKBtn.setTypeface(Utility.font_bold);

			noFavOKBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					noFavalert.dismiss();
				}
			});
	}
	}
	
	 boolean isEmailValid(CharSequence email) {
	     return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	  }

	@Override
	public boolean onLongClick(View v) {
		EditText ed = (EditText) v;
		int stringLength = ed.getText().length();
		boolean returnValue = Utility.copyPasteMethod2(v, stringLength);
		return returnValue;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent i = new Intent(SendAFriendNewActivity.this, SearchListActivity.class);
			i.putExtra("finish", true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
			startActivity(i);
			SendAFriendNewActivity.this.finish();
			headerTitle = null;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			MemoryManager.unbindDrawables(findViewById(R.id.sendAFriendRLID));
			unRegisterBaseActivityReceiver();
			Runtime.getRuntime().gc();
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}	
	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		try {
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}
}
