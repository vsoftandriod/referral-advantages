package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.cache.SmartImageView;
import com.myrewards.referraladvantage.model.Product;
import com.myrewards.referraladvantage.service.GrabItNowService;
import com.myrewards.referraladvantage.service.ServiceListener;
import com.myrewards.referraladvantage.utils.ApplicationConstants;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;

public class ResultsListActivity extends HomeBasedActivity implements
		OnItemClickListener, ServiceListener, 
		OnScrollListener { //IImageURLTaskListener
	List<Product> productsList;
	List<Product> productsListTemp;
	ResultAdapter mAdapter;
	LayoutInflater inflater;
	View mLoading;
	ListView resultsListView;
	String catID = null;
	String location = null;
	String keyword = null;
	private int mvisibleItemCount = -1;
	private String fetchDirectionUP = Utility.FETCH_DIRECTION_UP;
	private String fetchDirection = "";
	private int visibleThreshold = 0;
	private int previousTotal = 0;
	private boolean loading = true;
	private int mfirstVisibleItem;;

	int productsCount = 0;

	DatabaseHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.results_list);
		Utility.DailyDealsScreen = "null";
		Utility.FavouritesScreen ="null";
		Utility.hotOffersScreen = "null";
		Utility.AroundMeScreen = "null";
		
		try {
			registerBaseActivityReceiver();
			setHeaderTitle(getResources().getString(R.string.search_products_text));

			RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
			headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

			showBackButton();
			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			menuListView = (ListView) findViewById(R.id.menuListViewID);
			initialiseViews();
			dbHelper = new DatabaseHelper(this);
			mLoading = (View) findViewById(R.id.loading);
			resultsListView = (ListView) findViewById(R.id.resultsListViewID);
			productsList = new ArrayList<Product>();
			productsListTemp = new ArrayList<Product>();
			resultsListView.setOnItemClickListener(this);
			resultsListView.setOnScrollListener(this);
			if (getIntent() != null) {
				try {
					Bundle bundle = getIntent().getExtras();
					if (bundle != null) {
						catID = bundle.getString(ApplicationConstants.CAT_ID_KEY);
						location = bundle.getString(ApplicationConstants.LOCATION_KEY);
						keyword = bundle.getString(ApplicationConstants.KEYWORK_KEY);
					}
					if (Utility
							.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						if (catID != null) {
							GrabItNowService.getGrabItNowService().sendSearchProductsRequest(this, catID, location, keyword, 0, 30);
						} else {
							GrabItNowService.getGrabItNowService().sendHotOffersRequest(this);
						}
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));

						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
						ResultsListActivity.this.finish();
					}
				} catch (Exception e) {
					if (e != null) {
						e.printStackTrace();
						Log.w("HARI-->DEBUG", e);
					}
				}
			}
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View v) {
		try {
			if (v.getId() == R.id.backToSearchBtnID) {
				ResultsListActivity.this.finish();
			}
			super.onClick(v);
		} catch (NullPointerException e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	public class ResultAdapter extends BaseAdapter {

		public ResultAdapter(ResultsListActivity resultsListActivity) {

		}

		@Override
		public int getCount() {
			return productsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View resultsListRow = null;
			try {
				if (resultsListRow == null) {
					inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					resultsListRow = (View) inflater.inflate(
							R.layout.results_list_item, null, false);
				}
				LinearLayout rowLL2 = (LinearLayout) resultsListRow
						.findViewById(R.id.resultListItemHariLLID);
				rowLL2.getLayoutParams().height = (int) (Utility.screenHeight / 8.5);
				LinearLayout rowLL = (LinearLayout) resultsListRow
						.findViewById(R.id.resultItemLLID);
				TextView productNameTV = (TextView) resultsListRow
						.findViewById(R.id.productTVID);
				productNameTV.setTypeface(Utility.font_bold);

				TextView highlightTV = (TextView) resultsListRow
						.findViewById(R.id.offerTVID);
				highlightTV.setTypeface(Utility.font_reg);
				productNameTV.setText(productsList.get(pos).getName());
				highlightTV.setText(productsList.get(pos).getHighlight());
				
				switch (pos % 4) {
				case 0:
					rowLL.setBackgroundResource(R.color.result_color_one);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 1:
					rowLL.setBackgroundResource(R.color.result_color_two);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				case 2:
					rowLL.setBackgroundResource(R.color.result_color_three);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_one));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color));
					break;
				case 3:
					rowLL.setBackgroundResource(R.color.result_color_four);
					highlightTV.setTextColor(getResources().getColor(R.color.offer_text_color_two));
					productNameTV.setTextColor(getResources().getColor(R.color.product_name_text_color_two));
					break;
				}
			
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
					return null;
				}
			}
			return resultsListRow;
		}

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,
			long arg3) {
		super.onItemClick(arg0, rowView, pos, arg3);
		if (arg0.getId() == R.id.resultsListViewID) {
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					if (menuListView.getVisibility() == ListView.GONE) {
						Intent detailsIntent = new Intent(ResultsListActivity.this,	ProductDetailsActivity.class);
						detailsIntent.putExtra(ApplicationConstants.PRODUCT_ID_KEY,
								productsList.get(pos).getId());
						detailsIntent.putExtra(ApplicationConstants.COLOR_CODE_KEY,
								pos % 4);
						detailsIntent.putExtra(
								ApplicationConstants.PRODUCT_NAME_KEY, productsList
										.get(pos).getName());
						detailsIntent.putExtra(
								ApplicationConstants.PRODUCT_HIGHLIGHT_KEY,
								productsList.get(pos).getHighlight());
						detailsIntent.putExtra(Utility.FINISHED_STATUS, "RESULT_LIST_SCREEN");
						startActivity(detailsIntent);
					}
			}else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
							(ViewGroup) findViewById(R.id.custom_toast_layout_id));

					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					e.printStackTrace();
					Log.w("HARI-->DEBUG", e);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onServiceComplete(Object response, int eventType) {
		try {
			if (eventType != 16) {
				if (response != null) {
					if (response instanceof String) {
						mLoading.setVisibility(View.GONE);
						showDialog(1);
					} else {
						if(eventType==5 || eventType==6)
						{
						productsList = (ArrayList<Product>) response;

						productsCount = productsCount + productsList.size();

						if (!(productsCount > 0)) {

							mLoading.setVisibility(View.GONE);
							showDialog(1);

							if (Utility.user.getClientBanner() == null) {
								//ImageURLTask imgTask = new ImageURLTask();
								//imgTask.setListener(this);
								//imgTask.execute(ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id());
								String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
								newImagesLoading(bannerURL);
							} else {
								//onImageLoadComplete(Utility.user.getClientBanner());
							}
							mLoading.setVisibility(View.GONE);
						} else {
							if (Utility.user.getClientBanner() == null) {
								//ImageURLTask imgTask = new ImageURLTask();
								//imgTask.setListener(this);
								//imgTask.execute(ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id());
								String bannerURL = ApplicationConstants.CLIENT_BANNER_WRAPPER+ Utility.user.getClient_id();
								newImagesLoading(bannerURL);
							} else {
								//onImageLoadComplete(Utility.user.getClientBanner());
							}

							mLoading.setVisibility(View.GONE);

							if (fetchDirection
									.equalsIgnoreCase(fetchDirectionUP)) {

								int position = productsListTemp.size();
								productsListTemp.addAll(productsList);
								productsList = productsListTemp;

								mAdapter = new ResultAdapter(this);
								resultsListView.setAdapter(mAdapter);
								resultsListView.setScrollingCacheEnabled(false);
								if (productsList.size() > 0
										&& mvisibleItemCount != -1)
									resultsListView.setSelection(position
											- mvisibleItemCount + 2);
								else
									resultsListView.setSelection(position);
							}

							if (mAdapter == null) {
								mAdapter = new ResultAdapter(this);
								resultsListView.setAdapter(mAdapter);
							} else {
								mAdapter.notifyDataSetChanged();
							}
						}
					}
				}
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void newImagesLoading(String bannerURL) {
		// Loader image - will be shown before loading image
		SmartImageView banner = (SmartImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		//bannerIV.getLayoutParams().width = Utility.screenWidth * 1;
		
		//int loader = R.drawable.loading_imp;
		//int loader = bannerIV.getWidth();
		//Log.w("Hari-->", String.valueOf(loader));
		//int loader = bannerIV.getLayoutParams().width = Utility.screenWidth;
        
		// Image url
        String image_url = bannerURL;
        Log.w("Hari-->", bannerURL);
        
        // ImageLoader class instance
      //  BannerImageLoader imgLoader = new BannerImageLoader(getApplicationContext());
        
        // whenever you want to load an image from url
        // call DisplayImage function
        // url - image url to load
        // loader - loader image, will be displayed before getting image
        // image - ImageView 
        //imgLoader.DisplayImage(image_url, loader, bannerIV);
        try {
        	banner.setImageUrl(image_url);
        	//imgLoader.DisplayImage(image_url, bannerIV);
		} catch (OutOfMemoryError e) {
			if ( e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		try {
			mvisibleItemCount = visibleItemCount;
			if (firstVisibleItem > mfirstVisibleItem && !loading
					&& (firstVisibleItem + visibleItemCount == totalItemCount)
					&& totalItemCount > 0 && totalItemCount != visibleItemCount) {
				fetchDirection = fetchDirectionUP;
				System.out.println("firstVisibleItem" + firstVisibleItem);
				System.out.println("visibleItemCount" + visibleItemCount);
				System.out.println("totalItemCount" + totalItemCount);
				if (!loading
						&& (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)
						&& totalItemCount != 0
						&& mLoading.getVisibility() != View.VISIBLE) {
					sendRequestWithScrollDirection(fetchDirectionUP, totalItemCount);
					loading = true;
				}
			}

			if (loading) {
				if (totalItemCount > previousTotal) {
					loading = false;
					previousTotal = totalItemCount;

				}
			}
			mfirstVisibleItem = firstVisibleItem;
		
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	private void sendRequestWithScrollDirection(String DirectiontoFetch,
			int totalItemCount) {
		try {
			if (productsList.size() == totalItemCount) {
				mLoading.setVisibility(View.VISIBLE);
				fetchDirection = DirectiontoFetch;
				productsListTemp = productsList;

				if ((productsCount - 30) >= 0) {
					if (Utility
							.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
						GrabItNowService.getGrabItNowService()
								.sendSearchProductsRequest(this, catID, location,
										keyword, productsCount, 10);
					} else {
						// The Custom Toast Layout Imported here
						LayoutInflater inflater = getLayoutInflater();
						View layout = inflater
								.inflate(
										R.layout.toast_no_netowrk,
										(ViewGroup) findViewById(R.id.custom_toast_layout_id));

						// The actual toast generated here.
						Toast toast = new Toast(getApplicationContext());
						toast.setDuration(Toast.LENGTH_LONG);
						toast.setView(layout);
						toast.show();
						ResultsListActivity.this.finish();
					}
				} else {
					mLoading.setVisibility(View.GONE);
				}
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 1) {
				AlertDialog dialogDetails = null;
				LayoutInflater inflater = LayoutInflater.from(this);
				View noSearchResults = inflater.inflate(
						R.layout.dailog_layout_for_searchlist_noitem, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(noSearchResults);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				return dialogDetails;
			}

			else if (id == 13) {
				AlertDialog dialogDetails = null;

				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(R.layout.dialog_layout_logout,
						null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();
				dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				dialogDetails.show();
				return dialogDetails;

			}

			else if (id == 3) {
				AlertDialog noFavalert = null;

				LayoutInflater in1 = LayoutInflater.from(this);
				View dview = in1.inflate(R.layout.dialog_layout_fav_no, null);
				AlertDialog.Builder dbl1 = new AlertDialog.Builder(this);
				dbl1.setView(dview);
				noFavalert = dbl1.create();
				noFavalert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
				noFavalert.show();
				return noFavalert;
			}
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
			return null;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {
			if (id == 1) {
				final AlertDialog alertDialog2 = (AlertDialog) dialog;
				TextView alertTitle = (TextView) alertDialog2
						.findViewById(R.id.firstLoginCEPUTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				Button okbutton = (Button) alertDialog2
						.findViewById(R.id.nosearchResultsID);
				okbutton.setTypeface(Utility.font_bold);
				okbutton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog2.dismiss();
						ResultsListActivity.this.finish();
					}
				});
			}
			else if (id == 13) {
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				logoutText = (TextView) alertDialog1
						.findViewById(R.id.my_logoutTVID);

				TextView alertTitle = (TextView) alertDialog1
						.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				logoutText.setTypeface(Utility.font_reg);
				// logoutText.setVisibility(View.VISIBLE);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);

				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							if (helper != null) {
								helper.deleteLoginDetails();
								closeAllActivitiesByHari();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->Debug", e);
							}
						}
						Intent logoutIntent = new Intent(ResultsListActivity.this,
								LoginActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						logoutIntent.putExtra("finish", true);
						startActivity(logoutIntent);
						LoginActivity.etUserId.setText("");
						LoginActivity.etPasswd.setText("");
						LoginActivity.subDomainURL_et.setText("");
						ResultsListActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
			}
			else if (id == 3) {
				final AlertDialog noFavalert = (AlertDialog) dialog;

				TextView alertTitle = (TextView) noFavalert
						.findViewById(R.id.firstLoginGINTitleTVID);
				alertTitle.setTypeface(Utility.font_bold);

				noFavTV = (TextView) noFavalert.findViewById(R.id.nofavTVID);
				noFavTV.setTypeface(Utility.font_reg);
				noFavOKBtn = (Button) noFavalert.findViewById(R.id.noFavOkBtnID);
				noFavOKBtn.setTypeface(Utility.font_bold);

				noFavOKBtn.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						noFavalert.dismiss();
					}
				});
			}
			super.onPrepareDialog(id, dialog);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			ResultsListActivity.this.finish();
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onPause() {
        super.onPause();
        try {
            System.gc();
            Runtime.getRuntime().gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
     }

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			unRegisterBaseActivityReceiver();
			Runtime.getRuntime().gc();
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->", e);
			}
		}
	}	
	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		System.gc();
	}
}
