package com.myrewards.referraladvantage.controller;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.referraladvantage.model.Menu;
import com.myrewards.referraladvantage.model.User;
import com.myrewards.referraladvantage.utils.DatabaseHelper;
import com.myrewards.referraladvantage.utils.Utility;
import com.readystatesoftware.viewbadger.BadgeView;

public class HomeBasedActivity extends FragmentActivity implements OnItemClickListener,
		OnClickListener {
	private final int SEARCH_BY_CATEGORIES = 0;
	private final int WHAT_IS_AROUNDME = 1;
	private final int HOT_OFFERS = 2;
	protected final int MY_FAVOURITES = 3;
	private final int DAILY_DEALS = 4;
	private final int NOTICEBOARD = 5;
	private final int MY_MEMBERSHIP_CARD = 6;
	private final int SEND_A_FRIEND = 7;
	private final int BARCODE_SCANNER = 8;
	private final int PARKING_TIMER = 9;
	private final int FACEBOOK = 10;
	private final int TWITTER = 11;
	private final int HELP = 12;
	public final int LOGOUT = 13;
	private int SELECTED_MENU_ITEM;
	public static boolean noticeboardcount = false;

	static int noticeCount = 0;
	// this is from Login Activity
	Context context;
	DatabaseHelper helper;
	
	// logout
	public TextView logoutText;
	public Button loginbutton1, cancelbutton1;

	// no favorite found
	public TextView noFavTV;
	public Button noFavOKBtn;

	LayoutInflater inflater;
	MenuListAdapter mAdapter;
	ArrayList<Menu> menuList;
	public ListView menuListView;
	public Button menuBtn;
	public static ImageView unreadImage;
	public ImageView noticeboardunread;
	int imagesList[] = { R.drawable.menu_search, R.drawable.menu_around_me,
			R.drawable.menu_hot_offers, R.drawable.menu_my_favourites,
			R.drawable.menu_daily_deals, R.drawable.menu_notice_board,
			R.drawable.menu_membership_card, R.drawable.menu_send_a_frnd,
			R.drawable.menu_scan_icon, R.drawable.parking_menu,
			R.drawable.menu_facebook, R.drawable.menu_twitter,
			R.drawable.menu_help, R.drawable.menu_logout };
	User user;
	Typeface font_bold,font_reg;
	Bundle bundle = null;
	boolean finishState_logout;
	public static String currentScreen = null;
	
	public static final String FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION = "com.myrewards.grabitnow2.controller.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION";
	private BaseActivityReceiver baseActivityReceiver = new BaseActivityReceiver();
	public static final IntentFilter INTENT_FILTER = createIntentFilter();

	private static IntentFilter createIntentFilter(){
		IntentFilter filter = null;
		try {
			filter = new IntentFilter();
			filter.addAction(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION);
			
		} catch (Exception e) {
			filter = null;
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
		}
		return filter;
	}
	
	protected void registerBaseActivityReceiver() {
		registerReceiver(baseActivityReceiver, INTENT_FILTER);
	}
	
	protected void unRegisterBaseActivityReceiver() {
		unregisterReceiver(baseActivityReceiver);
	}
	
	public class BaseActivityReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			try {
				if(intent.getAction().equals(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION)){
					finish();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
				}
			}
		}
	} 
	
	protected void closeAllActivitiesByHari(){
		try {
			sendBroadcast(new Intent(FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION));
			System.gc();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}
	
	public void bannerVisibilty(boolean isVisible) {
		ImageView banner = (ImageView) findViewById(R.id.bannerIVID);
		banner.getLayoutParams().height = (int) (Utility.screenHeight / 11.0);
		//banner.getLayoutParams().height = Utility.screenHeight / 10;
		if (isVisible) {
			banner.setVisibility(View.VISIBLE);
		} else {
			banner.setVisibility(View.GONE);
		}
	}

	public void setHeaderTitle(String title) {
		TextView titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);
		titleTV.setText(title);
		currentScreen = title;
	}

	public void showBackButton() {
		Button backBtn = (Button) findViewById(R.id.backToSearchBtnID);
		backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backBtn.setVisibility(View.VISIBLE);
		backBtn.setOnClickListener(this);
	}

	public void initialiseViews() 
	{
		try {
			menuList = new ArrayList<Menu>();
			for (int i = 0; i < imagesList.length; i++) 
			{
				Menu menu = new Menu();
				menu.setImageResourceID(imagesList[i]);
				menu.setName(getResources().getStringArray(R.array.menuStrings)[i]);
				menuList.add(menu);
			}
			menuBtn = (Button) findViewById(R.id.menuBtnID);
			menuBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			menuBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			menuBtn.setOnClickListener(this);
			menuListView.setOnItemClickListener(this);
			mAdapter = new MenuListAdapter(this);
			menuListView.setAdapter(mAdapter);
		} catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	public class MenuListAdapter extends BaseAdapter {

		public MenuListAdapter(HomeBasedActivity menuListActivity) {

		}

		@Override
		public int getCount() {
			return menuList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int pos, View view, ViewGroup arg2) {
			View menuListRow = null;
			if (menuListRow == null) {
				inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				menuListRow = (ViewGroup) inflater.inflate(R.layout.menu_list_item, null, false);
			}

			RelativeLayout menuRL = (RelativeLayout) menuListRow.findViewById(R.id.menuDeviceListRLID);
			menuRL.getLayoutParams().height=(int) (Utility.screenHeight/13.8);
			TextView menuItemName = (TextView) menuListRow.findViewById(R.id.menuItemTVID);
			menuItemName.setTypeface(Utility.font_bold);
			menuItemName.setText(menuList.get(pos).getName());
			menuItemName.setTextColor(Color.parseColor("#FFFFFF"));
			ImageView menuItemImage = (ImageView) menuListRow.findViewById(R.id.menuItemIVID);
			menuItemImage.getLayoutParams().width = (int) (Utility.screenWidth / 9);
			menuItemImage.getLayoutParams().height = (int) (Utility.screenHeight / 15.0);
			menuItemImage.setImageResource(menuList.get(pos).getImageResourceID());

			if (pos == 5) {
				helper = new DatabaseHelper(HomeBasedActivity.this);
				noticeboardunread = (ImageView) menuListRow.findViewById(R.id.menuItemUnreadIVID);
				BadgeView badge = new BadgeView(HomeBasedActivity.this, noticeboardunread);
				if (helper.getNoticeDetails() != 0) {
					badge.setText(Integer.toString(helper.getNoticeDetails()));
					badge.setTextColor(getResources().getColor(R.color.result_color_two));
					badge.setBadgeBackgroundColor(Color.parseColor("#FFFFFF"));
					badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
					badge.show();
				} else {
					badge.setVisibility(View.GONE);
					noticeboardunread.setVisibility(View.GONE);
				}
				// }
			}

			View line = (View) menuListRow.findViewById(R.id.lineID);
			View lineBig = (View) menuListRow.findViewById(R.id.lineBigID);
			if (pos == 0)
				line.setVisibility(View.GONE);
			
			lineBig.setVisibility(View.GONE);
			line.getLayoutParams().width = 5 * (Utility.screenWidth / 6);
			/*if(pos==7)
			{
				menuRL.setVisibility(View.GONE);
			}*/
			// }
			return menuListRow;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View rowView, int pos,	long arg3) {
		try {
			if (menuListView.getVisibility() == ListView.VISIBLE) {
					switch (pos) {
					case SEARCH_BY_CATEGORIES:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = SEARCH_BY_CATEGORIES;
								setForHeadersTitles(SELECTED_MENU_ITEM);
								//This is HARI logic  
								if (SearchListActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(SearchListActivity.headerTitle) || Utility.ResultScreen.equals("RESULT_LIST_SCREEN")) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent searchCategoriesIntent = new Intent(HomeBasedActivity.this, SearchListActivity.class);
										startActivity(searchCategoriesIntent);
									}
								} else {
									Intent searchCategoriesIntent = new Intent(HomeBasedActivity.this, SearchListActivity.class);
									startActivity(searchCategoriesIntent);
								}
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("HARI-->DEBUG", e);
							}
						}
						break;
					case WHAT_IS_AROUNDME:// Revanth B00604 Tarun B00603 
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = WHAT_IS_AROUNDME;
								//This is HARI logic  
								setForHeadersTitles(SELECTED_MENU_ITEM);
								if ((WhatsAroundMeDudeActivity.headerTitle != null && currentScreen != null)) {
									if (currentScreen.equals(WhatsAroundMeDudeActivity.headerTitle) || Utility.AroundMeScreen.equals("AROUND_ME_SCREEN")) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent mapIntent = new Intent(HomeBasedActivity.this, WhatsAroundMeDudeActivity.class);
										startActivity(mapIntent);
									}
								} else {
									Intent mapIntent = new Intent(HomeBasedActivity.this, WhatsAroundMeDudeActivity.class);
									startActivity(mapIntent);
								}
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("HARI-->DEBUG", e);
							}
						}
						break;
					case HOT_OFFERS:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = HOT_OFFERS;
								//This is HARI logic  
								setForHeadersTitles(SELECTED_MENU_ITEM);
								if (HotOffersActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(HotOffersActivity.headerTitle) || Utility.hotOffersScreen.equals("HOT_OFFERS_SCREEN")) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent hotOffersIntent = new Intent(HomeBasedActivity.this, HotOffersActivity.class);
										startActivity(hotOffersIntent);
									}
								} else {
									Intent hotOffersIntent = new Intent(HomeBasedActivity.this, HotOffersActivity.class);
									startActivity(hotOffersIntent);
								}
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("HARI-->DEBUG", e);
							}
						}
						break;
					case MY_FAVOURITES:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = MY_FAVOURITES;
								setForHeadersTitles(SELECTED_MENU_ITEM);
								DatabaseHelper dbHelper = new DatabaseHelper(getBaseContext());
								if (dbHelper.getProductList().size() > 0) {
									//This is HARI logic  
								if (FavoritesListActivity.headerTitle != null && currentScreen != null) {
										if (currentScreen.equals(FavoritesListActivity.headerTitle) || Utility.FavouritesScreen.equals("FAVOURITES_SCREEN")) {
											if (menuListView.getVisibility() == View.VISIBLE) {
												applyMenuListSlideAnimation(0, -1);
												menuListView.setVisibility(View.GONE);
											}
										} else {
											Intent myFavoritesIntent = new Intent(HomeBasedActivity.this, FavoritesListActivity.class);
											startActivity(myFavoritesIntent);
										}
									} else {
										Intent myFavoritesIntent = new Intent(HomeBasedActivity.this, FavoritesListActivity.class);
										startActivity(myFavoritesIntent);
									}
								} else {
									showDialog(MY_FAVOURITES);
								}
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								e.printStackTrace();
								Log.w("HARI-->DEBUG", e);
							}
						}
						break;
					case DAILY_DEALS:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = DAILY_DEALS;
								//This is HARI logic  
								setForHeadersTitles(SELECTED_MENU_ITEM);
								if (DailyDealsActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(DailyDealsActivity.headerTitle) || Utility.DailyDealsScreen.equals("DAIYLY_DEALS_SCREEN")) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent dailyDealsIntent = new Intent(HomeBasedActivity.this,	DailyDealsActivity.class);
										startActivity(dailyDealsIntent);
									}
								} else {
									Intent dailyDealsIntent = new Intent(HomeBasedActivity.this,	DailyDealsActivity.class);
									startActivity(dailyDealsIntent);
								}
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case NOTICEBOARD:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								noticeboardcount = true;
								if (noticeCount <= 1) {
									noticeCount = 1;
								}
								SELECTED_MENU_ITEM = NOTICEBOARD;
								//This is HARI logic  
								setForHeadersTitles(SELECTED_MENU_ITEM);
								if (NoticeBoardActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(NoticeBoardActivity.headerTitle)) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent noticeBoardIntent = new Intent(HomeBasedActivity.this, NoticeBoardActivity.class);
										noticeBoardIntent.putExtra("NOTICE_SPECIAL_COUNT", 3000);
										startActivity(noticeBoardIntent);
									}
								} else {
									Intent noticeBoardIntent = new Intent(HomeBasedActivity.this, NoticeBoardActivity.class);
									noticeBoardIntent.putExtra("NOTICE_SPECIAL_COUNT", 3000);
									startActivity(noticeBoardIntent);
								}
							} else {
								// The Custom Toast Layout Imported here --> by HARI
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case HELP:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = HELP;
								//This is HARI logic  
								setForHeadersTitles(SELECTED_MENU_ITEM);
								if (HelpPagesActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(HelpPagesActivity.headerTitle)) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent helpIntent = new Intent(HomeBasedActivity.this, HelpPagesActivity.class);
										startActivity(helpIntent);
									}
								} else {
									Intent helpIntent = new Intent(HomeBasedActivity.this, HelpPagesActivity.class);
									startActivity(helpIntent);
								}
							} else {
								// The Custom Toast Layout Imported here --> by HARI
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case MY_MEMBERSHIP_CARD:
						try {
							
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = MY_MEMBERSHIP_CARD;
								//This is HARI logic  
								setForHeadersTitles(SELECTED_MENU_ITEM);
								if (MyMemberShipCardActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(MyMemberShipCardActivity.headerTitle)) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent membershipCardIntent = new Intent(HomeBasedActivity.this, MyMemberShipCardActivity.class);
										startActivity(membershipCardIntent);
									}
								} else {
									Intent membershipCardIntent = new Intent(HomeBasedActivity.this, MyMemberShipCardActivity.class);
									startActivity(membershipCardIntent);
								}
							} else {
								// The Custom Toast Layout Imported here --> by HARI
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case SEND_A_FRIEND:
						try {
							SELECTED_MENU_ITEM = SEND_A_FRIEND;
							// This is HARI logic
							if (SendAFriendNewActivity.headerTitle != null
									&& currentScreen != null) {
								if (currentScreen
										.equals(SendAFriendNewActivity.headerTitle)) {
									if (menuListView.getVisibility() == View.VISIBLE) {
										applyMenuListSlideAnimation(0, -1);
										menuListView.setVisibility(View.GONE);
									}
								} else {
									Intent parking_timer = new Intent(
											HomeBasedActivity.this,
											SendAFriendNewActivity.class);
									startActivity(parking_timer);
								}
							} else {
								Intent parking_timer = new Intent(
										HomeBasedActivity.this,
										SendAFriendNewActivity.class);
								startActivity(parking_timer);
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case BARCODE_SCANNER:
						SELECTED_MENU_ITEM = BARCODE_SCANNER;
						Intent bar_code_scanner = new Intent(HomeBasedActivity.this,	BarCodeScannerActivity.class);
						startActivity(bar_code_scanner);
						currentScreen = null;
						break;
					case PARKING_TIMER:
						try {
							SELECTED_MENU_ITEM = PARKING_TIMER;
								//This is HARI logic  
							setForHeadersTitles(SELECTED_MENU_ITEM);
								if (NewParkingTimeActivity.headerTitle != null && currentScreen != null) {
									if (currentScreen.equals(NewParkingTimeActivity.headerTitle)) {
										if (menuListView.getVisibility() == View.VISIBLE) {
											applyMenuListSlideAnimation(0, -1);
											menuListView.setVisibility(View.GONE);
										}
									} else {
										Intent parking_timer = new Intent(HomeBasedActivity.this, NewParkingTimeActivity.class);
										startActivity(parking_timer);
									}
								} else {
									Intent parking_timer = new Intent(HomeBasedActivity.this, NewParkingTimeActivity.class);
									startActivity(parking_timer);
								}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case FACEBOOK:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								SELECTED_MENU_ITEM = FACEBOOK;
								Intent i = new Intent(Intent.ACTION_VIEW);
								i.setData(Uri.parse("https://www.facebook.com/myrewardsinternational"));
								startActivity(i);
								currentScreen = null;
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk,
								(ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;
					case TWITTER:
						try {
							if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
								setForHeadersTitles(SELECTED_MENU_ITEM);
								Intent in = new Intent(Intent.ACTION_VIEW);
								in.setData(Uri.parse("https://twitter.com/MyRewardsIntl"));
								startActivity(in);
								currentScreen = null;
							} else {
								// The Custom Toast Layout Imported here
								LayoutInflater inflater = getLayoutInflater();
								View layout = inflater.inflate(R.layout.toast_no_netowrk, (ViewGroup) findViewById(R.id.custom_toast_layout_id));
											 
								// The actual toast generated here.
								Toast toast = new Toast(getApplicationContext());
								toast.setDuration(Toast.LENGTH_LONG);
								toast.setView(layout);
								toast.show();
							}
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
							}
						}
						break;

					case LOGOUT:
						SELECTED_MENU_ITEM = LOGOUT;
						showDialog(LOGOUT);
						break;
					default:
						break;
				}
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	private void setForHeadersTitles(int sELECTED_MENU_ITEM2) {
		switch (sELECTED_MENU_ITEM2) {
		case SEARCH_BY_CATEGORIES:
			if (currentScreen !=null) {
				if (!currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case WHAT_IS_AROUNDME:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case HOT_OFFERS:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case MY_FAVOURITES:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case DAILY_DEALS:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case NOTICEBOARD:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case MY_MEMBERSHIP_CARD:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}
			break;
		case PARKING_TIMER:
			if (currentScreen !=null) {
				if (currentScreen.equals("Search")) {
					currentScreen = null;
				}
			}	
			break;

		default:
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		try {
			if (id == 13) {
				AlertDialog dialogDetails = null;
				switch (id) {
				case LOGOUT:
					LayoutInflater inflater = LayoutInflater.from(this);
					View dialogview = inflater.inflate(
							R.layout.dialog_layout_logout, null);
					AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
							this);
					dialogbuilder.setCancelable(false);
					dialogbuilder.setView(dialogview);
					dialogDetails = dialogbuilder.create();
					dialogDetails.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
					dialogDetails.show();

					break;
				}
				return dialogDetails;
			} else if (id == 3) {
				AlertDialog noFavalert = null;
				switch (id) {
				case MY_FAVOURITES:
					LayoutInflater in1 = LayoutInflater.from(this);
					View dview = in1.inflate(R.layout.dialog_layout_fav_no, null);
					AlertDialog.Builder dbl1 = new AlertDialog.Builder(this);
					dbl1.setView(dview);
					noFavalert = dbl1.create();
					noFavalert.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationByHari;
					noFavalert.show();
					break;
				}
				return noFavalert;
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
			return null;
		}
		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		try {

			if (id == LOGOUT) {
					final AlertDialog alertDialog1 = (AlertDialog) dialog;
					
					TextView alertTitle=(TextView)alertDialog1.findViewById(R.id.firstLoginGINTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);
					
					logoutText = (TextView)alertDialog1. findViewById(R.id.my_logoutTVID);
					
					logoutText.setTypeface(Utility.font_reg);
					
					// logoutText.setVisibility(View.VISIBLE);
					loginbutton1 = (Button) alertDialog1
							.findViewById(R.id.loginBtnH_ID);
					loginbutton1.setTypeface(Utility.font_bold);
					cancelbutton1 = (Button) alertDialog1
							.findViewById(R.id.btn_cancelH_ID);
					cancelbutton1.setTypeface(Utility.font_bold);
					alertDialog1.setCancelable(false);

					loginbutton1.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							try {
								if (helper != null) {
									helper.deleteLoginDetails();
									closeAllActivitiesByHari();
								}
							} catch (Exception e) {
								if (e != null) {
									Log.w("Hari-->Debug", e);
								}
							}
							Intent logoutIntent = new Intent(HomeBasedActivity.this, LoginActivity.class);
							logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
							//logoutIntent.putExtra("finish", true);
							startActivity(logoutIntent);
							LoginActivity.etUserId.setText("");
							LoginActivity.etPasswd.setText("");
							LoginActivity.subDomainURL_et.setText("");
							HomeBasedActivity.this.finish();
							alertDialog1.dismiss();
						}
					});
					cancelbutton1.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							alertDialog1.dismiss();
						}
					});
			} else if (id == MY_FAVOURITES) {
					final AlertDialog noFavalert = (AlertDialog) dialog;
					TextView alertTitle = (TextView) noFavalert.findViewById(R.id.firstLoginGINTitleTVID);
					alertTitle.setTypeface(Utility.font_bold);

					noFavTV = (TextView) noFavalert.findViewById(R.id.nofavTVID);
					noFavTV.setTypeface(Utility.font_reg);
					noFavOKBtn = (Button) noFavalert
							.findViewById(R.id.noFavOkBtnID);
					noFavOKBtn.setTypeface(Utility.font_bold);
					noFavalert.setCancelable(false);
					noFavOKBtn.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							noFavalert.dismiss();
						}
					});
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.menuBtnID:
			if (menuListView.getVisibility() == View.GONE) {
				menuListView.setVisibility(View.VISIBLE);
				applyMenuListSlideAnimation(-1, 0);
			} else {
				applyMenuListSlideAnimation(0, -1);
				menuListView.setVisibility(View.GONE);
			}
			break;
		case R.id.backToSearchBtnID:
			Button backBtn = (Button) findViewById(R.id.backToSearchBtnID);
			backBtn.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
			backBtn.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
			backBtn.setVisibility(View.INVISIBLE);
			HomeBasedActivity.this.finish();
			//ProductDetailsActivity.headerTitle = null;
			break;
		}
	}

	public void applyMenuListSlideAnimation(float start, float end) {
		TranslateAnimation translate = new TranslateAnimation(
				TranslateAnimation.RELATIVE_TO_SELF, 0,
				TranslateAnimation.RELATIVE_TO_SELF, 0,
				TranslateAnimation.RELATIVE_TO_SELF, start,
				TranslateAnimation.RELATIVE_TO_SELF, end);
		translate.setDuration(500);
		translate.setStartOffset(500);
		menuListView.startAnimation(translate);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			HomeBasedActivity.this.finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
