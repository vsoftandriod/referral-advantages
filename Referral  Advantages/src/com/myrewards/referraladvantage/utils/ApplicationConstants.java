package com.myrewards.referraladvantage.utils;

public class ApplicationConstants {
	
//	public static String subDomainURLString = "www.referraladvantage.com.au";
	
	public static String subDomainURLString = "www.motoronerewards.com.au";
	
	
	public static String SERVER_ROOT = "http://www.myrewards.com.au/newapp/";
	public static final String LOGIN_WRAPPER = SERVER_ROOT + "login.php";
	public static final String FIRSTTIME_LOGIN = SERVER_ROOT+"first_time_login.php?";
	public static final String FIRST_TIME_USER_URL = SERVER_ROOT+"first_time_user.php";
	public static final String USER_DETAILS_WRAPPER = SERVER_ROOT+ "get_user.php?uname=";
	public static final String CLIENT_BANNER_WRAPPER = SERVER_ROOT+ "get_client_banner.php?cid=";
	public static final String CATEGORY_WRAPPER = SERVER_ROOT + "get_cat.php?";
	public static final String HOT_OFFERS_WRAPPER = SERVER_ROOT	+ "get_hot_offer.php?cid=";
	public static final String SEARCH_PRODUCTS_WRAPPER = SERVER_ROOT+ "search.php?";
	public static final String PRODUCT_DETAILS_WRAPPER = SERVER_ROOT+ "get_product.php?";
	public static final String NEAREST_LAT_LON_WRAPPER = SERVER_ROOT+ "get_products_by_loc.php?";
	public static final String PRODUCT_ADDRESSES_WRAPPER = SERVER_ROOT+ "get_product_addresses.php?pid=";
	public static final String HELP_SCREEN_WRAPPER = SERVER_ROOT+"get_page.php?cid=24&pid=61";
	public static final String REDEEM_DETAILS_WRAPPER = SERVER_ROOT+"redeemed.php";
	
	public static final String NOTICE_BOARD_WRAPPER = SERVER_ROOT+"get_notice.php?cid=94";
	public static final String NOTICEBOARD_ID_URL = SERVER_ROOT+"get_notice_ids.php?cid=94";
//	public static final String NOTICE_BOARD_WRAPPER = SERVER_ROOT+"get_notice.php?cid=1";
//	public static final String NOTICEBOARD_ID_URL = SERVER_ROOT+"get_notice_ids.php?cid=1";
	
	public static final String DAILY_DEALS_WRAPPER = SERVER_ROOT+"get_daily_deal.php?";
	
	
	public static final String SEND_A_FRIEND_ID_URL = SERVER_ROOT+ "saf.php";
	
	public static final String PRODUCT_LOGO_WRAPPER = "http://www.myrewards.com.au/files/merchant_logo/";
	public static final String PRODUCT_IMAGE_LOGO_WRAPPER = "http://www.myrewards.com.au/files/product_image/";
	public static final String MEMBERSHIP_CARD_WRAPPER = "http://www.myrewards.com.au/files/mobile_membership_card/";
	public static final String DAILY_DEALS_IMAGE_WRAPPER = "http://www.myrewards.com.au/files/hot_offer_image/"; //
		
	public static String UNABLETOESTABLISHCONNECTION = null;
	public static String UNABLETOESTABLISHCONNECTION_URL = null;
	public static String USERNAME_PASSWORD_FIELDS = null;
	
	public static final String CATEGORY_LIST = "getCategoryList";
	public static final String CAT_ID_KEY = "catIdKey";
	public static final String LOCATION_KEY = "locationKey";
	public static final String KEYWORK_KEY = "keywordKey";
	public static final String PRODUCT_ID_KEY = "productIdKey";
	public static final String COLOR_CODE_KEY = "ColorCodeKey";
	public static final String PRODUCT_NAME_KEY = "productNameKey";
	public static final String PRODUCT_HIGHLIGHT_KEY = "productHighlightKey";
	public static final String NOTICE_ID_KEY = "productIdKey";
	public static final String NOTICE_NAME_KEY = "productNameKey";
	public static final String NOTICE_DETAILS_KEY = "productHighlightKey";
	public static final String PHP_SESSION_KEY = "PHPSESSID=";
	public static final String DAILYDEALSIMAGE = "IMAGE";
	public static final String IMAGE_URL_KEY = "imageURL";
}
