package com.myrewards.referraladvantage.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.myrewards.referraladvantage.model.LoginDetails;
import com.myrewards.referraladvantage.model.Product;

//import com.myrewards.grabitnow.model.NoticeId;

@SuppressWarnings("unused")
public class DatabaseHelper extends SQLiteOpenHelper {
	static final String dbName = "Referrel Advantage";
	static final String loginTable = "loginNames";
	static final String favoriteTable = "product";
	static final String name = "Username";
	static final String pwd = "Password";
	static final String subDomain = "subDomain";
	static final String date = "loginDate";
	static final String productId = "productId";
	static final String productName = "productName";
	static final String productHighlight = "productHighlight";
	static final String noticeboardid = "noticeboardiddetails";
	static final String noticeId = "noticeid";
	static final String noticerealId = "noticerealid";
	static final String statusId = "status";

	// this table is for notices update on or off

	static final String noticeUpdateStateTable = "noticeupdatestatetable";
	static final String noticeUpdateState = "noticeupdatestate";

	public DatabaseHelper(Context context) {
		super(context, dbName, null,1);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL("CREATE TABLE " + loginTable + " (" + name + " TEXT, "
					+ pwd + " TEXT, " + subDomain + " TEXT, " + date + " TEXT)");
			db.execSQL("CREATE TABLE " + favoriteTable + " (" + productId
					+ " TEXT, " + productName + " TEXT, " + productHighlight
					+ " TEXT)");

			db.execSQL("CREATE TABLE " + noticeboardid + " (" + noticeId
					+ " TEXT, " + noticerealId + " TEXT, " + statusId
					+ " TEXT)");

			db.execSQL("CREATE TABLE " + noticeUpdateStateTable + "("
					+ noticeUpdateState + " TEXT)");
		} catch (SQLException e) {
			if (e != null) {
				Log.w("HARI-->DEBUG", e);
			}
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		try {
			db.execSQL("DROP TABLE IF EXISTS " + loginTable);
			db.execSQL("DROP TABLE IF EXISTS " + favoriteTable);
			db.execSQL("DROP TABLE IF EXISTS " + noticeboardid);
			db.execSQL("DROP TABLE IF EXISTS " + noticeUpdateStateTable);
			onCreate(db);
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	public void addLoginDetails(String username, String password,
			String subDomain1, String loginDate) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + loginTable);
		ContentValues cv = new ContentValues();
		try {
			cv.put(name, username);
			cv.put(pwd, password);
			cv.put(subDomain, subDomain1);
			cv.put(date, loginDate);

			db.insert(loginTable, name, cv);
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	public void deleteLoginDetails() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + loginTable);
	}

	public void addFavoriteProductDetails(String id, String name,
			String highlight) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.execSQL("DELETE FROM "+loginTable);
		ContentValues cv = new ContentValues();
		try {
			cv.put(productId, id);
			cv.put(productName, name);
			cv.put(productHighlight, highlight);

			db.insert(favoriteTable, productId, cv);
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	public LoginDetails getLoginDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db
				.rawQuery("SELECT * FROM " + loginTable, new String[] {});
		LoginDetails loginDetails = null;
		try {
			if (cur.moveToFirst()) {
				do {
					loginDetails = new LoginDetails();
					loginDetails.setUsername(cur.getString(cur
							.getColumnIndex(name)));
					loginDetails.setPassword(cur.getString(cur
							.getColumnIndex(pwd)));
					loginDetails.setSubDomain(cur.getString(cur
							.getColumnIndex(subDomain)));
					loginDetails.setLoginDate(cur.getString(cur
							.getColumnIndex(date)));
				} while (cur.moveToNext());
			}
			cur.close();
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
		if (loginDetails != null) {
			return loginDetails;
		} else {
			return null;
		}

	}

	public List<Product> getProductList() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + favoriteTable,
				new String[] {});
		int i = 0;
		List<Product> productsList = new ArrayList<Product>();
		try {
			if (cur.moveToFirst()) {
				do {
					Product product = new Product();
					String id = cur.getString(cur.getColumnIndex(productId));
					product.setId(Integer.parseInt(id));
					product.setName(cur.getString(cur
							.getColumnIndex(productName)));
					product.setHighlight(cur.getString(cur
							.getColumnIndex(productHighlight)));
					productsList.add(product);
					i++;
				} while (cur.moveToNext());
			}
			cur.close();
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
		return productsList;
	}

	public boolean isProductExist(String productId1) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT " + productId + " FROM "
				+ favoriteTable, new String[] {});
		String productIdStored = null;
		try {
			if (cur.moveToFirst()) {
				do {
					productIdStored = cur.getString(cur
							.getColumnIndex(productId));
					if (productIdStored.equals(productId1)) {
						return true;
					}
				} while (cur.moveToNext());
			}
			cur.close();
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
		return false;
	}

	public String getPasswd(String uName) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db
				.rawQuery("SELECT * FROM " + loginTable, new String[] {});
		String passwd = null;
		try {
			if (cur.moveToFirst()) {
				String username = null;
				do {
					username = cur.getString(cur.getColumnIndex(name));
					if (username.equals(uName)) {
						passwd = cur.getString(cur.getColumnIndex(pwd));
					}
				} while (cur.moveToNext());
			}
			cur.close();
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
		return passwd;
	}

	public String getUrl() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT * FROM " + favoriteTable, null);
		String urlString = null;
		try {
			if (cur.moveToFirst()) {

				do {
					urlString = cur.getString(cur.getColumnIndex(subDomain));

				} while (cur.moveToNext());
			}
			cur.close();
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
		return urlString;
	}

	public void updatePasswd(String userId, String passwd) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		try {
			cv.put(name, userId);
			cv.put(pwd, passwd);
			db.update(loginTable, cv, name + "='" + userId + "'", null);
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	private void addUsernameDetails(String username) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			db.execSQL("DELETE FROM " + loginTable);
			ContentValues cv = new ContentValues();
			cv.put(name, username);
			db.insert(loginTable, name, cv);
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	private void updateUsername(String userID) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(name, userID);
		db.update(loginTable, cv, name + "='" + userID + "'", null);
		System.out.println("username in updateUsername():     " + userID);
	}

	public void clearPassword(String userName) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(loginTable,pwd,name+"="+getUserNames());
		db.close();
	}

	public void deleteProduct(String productId2) {
		SQLiteDatabase db = this.getWritableDatabase();
		// db.delete(loginTable,pwd,name+"="+getUserNames());
		db.execSQL("DELETE FROM " + favoriteTable + " WHERE " + productId + "="
				+ productId2);
		db.close();
	}

	// Notice Board Added details
	public void addnoticeiddetails(String noticeids) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("noticerealId", noticeids);
		cv.put(statusId, "OPEN");
		db.insert(noticeboardid, null, cv);
		db.close();
	}

	public void updateNoticeDetails(String p) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		try {
			cv.put(statusId, "CLOSE");
			db.update(noticeboardid, cv, noticerealId + "='" + p + "'", null);
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	public void deaActivateNoticeDetails(String p) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		try {
			cv.put(statusId, "DEACTIVATED");
			db.update(noticeboardid, cv, noticerealId + "='" + p + "'", null);
			db.close();
		} catch (Exception e) {
			if (e != null) {
				Log.w("HARI__>DEBUG", e);
			}
		}
	}

	public int getNoticeDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		int a = 0;
		try {
			Cursor cur = db.rawQuery("SELECT * FROM " + noticeboardid
					+ "  WHERE " + statusId + " = " + "'OPEN'", null);

			// Cursor cur =
			// db.rawQuery("SELECT * FROM "+noticeboardid+" WHERE "+
			// statusId +"="+"OPEN", new String [] {});
			a = cur.getCount();
			cur.close();
			db.close();
		} catch (Exception e) {
		}
		return a;
	}

	public List<String> getExistingNoticeRealIDs() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<String> list = new ArrayList<String>();
		Cursor c = db.rawQuery("SELECT * FROM " + noticeboardid, null);
		try {
			if (c.moveToFirst()) {
				do {
					list.add(c.getString(1));
				} while (c.moveToNext());
			}
			if (c != null && !c.isClosed()) {
				c.close();
			}
			c.close();
			db.close();
		} catch (Exception e) {
		}
		return list;
	}

	public void deleteNoticeIdDetails(String noticeId) {
		SQLiteDatabase db = this.getWritableDatabase();
		try {
			db.execSQL("DELETE FROM " + noticeboardid + " WHERE "
					+ noticerealId + "=" + noticeId);
			db.close();
		} catch (Exception e) {
		}
	}

	public String getNoticeIdReadorUnread(String pos) {
		String b = "CLOSE";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cur = db.rawQuery("SELECT " + statusId + " FROM "
				+ noticeboardid + "  WHERE " + noticerealId + " = " + pos,
				new String[] {});
		if (cur.moveToFirst()) {
			if (cur.getString(0).equals("CLOSE")) {
				b = "CLOSE";
			} else if (cur.getString(0).equals("OPEN")) {
				b = "OPEN";
			}

		}
		cur.close();
		db.close();
		return b;
	}

	public List<String> getExistingIDs() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<String> list = new ArrayList<String>();
		Cursor c = db.rawQuery("SELECT * FROM " + noticeboardid, null);
		if (c.moveToFirst()) {
			do {
				list.add(c.getString(1));
			} while (c.moveToNext());
		}
		if (c != null && !c.isClosed()) {
			c.close();
		}
		c.close();
		db.close();
		return list;
	}

	// this is for notice updates

	public void setNoticeUpdateState(String state) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + noticeUpdateStateTable);
		try {
			ContentValues cv = new ContentValues();
			cv.put(noticeUpdateState, state);
			db.insert(noticeUpdateStateTable, null, cv);
			db.close();
		} catch (Exception e) {
		}
	}

	public String getNoticeUpdateState() {
		String status = "NO_VALUE";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("SELECT " + noticeUpdateState + " FROM "
				+ noticeUpdateStateTable, null);
		try {
			cursor.moveToFirst();

			if (cursor.getCount() > 0) {
				status = cursor.getString(0);

			}
			cursor.close();
			db.close();
		} catch (Exception e) {

		}
		return status;
	}

	// ---------------------------------- //
}
