package com.myrewards.referraladvantage.utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.SystemClock;
import android.view.View;

import com.myrewards.referraladvantage.controller.NoticeBoardReceiver;
import com.myrewards.referraladvantage.model.Product;
import com.myrewards.referraladvantage.model.User;

public class Utility {
	public static int screenHeight;
	public static int screenWidth;
	public static String COOKIE=null;
	public static User user = null;
	public static Double mLat;	
	public static Double mLng;
	public static ArrayList<Product> productsListAroundMe = null;
	public static final String FETCH_DIRECTION_UP = "up";
	public static NetworkInfo netInfo;
	
	public static int fav_position_var;
	
	public static String user_Name;
	public static String user_Password;
	public static String user_website;
	
	public static Typeface font_bold,font_reg;
	
	public static final String FINISHED_STATUS = "ProductDetails"; 
	
	public static String AroundMeScreen = "null";
	public static String hotOffersScreen = "null";
	public static String FavouritesScreen = "null";
	public static String DailyDealsScreen = "null";
	public static String ResultScreen = "null";
	//public static String hotOffersScreen = "null";
	
	//this is for notification update
	
		public static AlarmManager alarmMgr0=null;
		 public static PendingIntent pendingIntent0=null;
	
	/*public static void showMessage(Context ctx, String msg) {
		Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
	}*/

	public static boolean isOnline(ConnectivityManager cm) {
		   /*ConnectivityManager cm =
        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);*/
    netInfo = cm.getActiveNetworkInfo();
    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
        return true;
    }
    return false;
}
	
	public static boolean copyPasteMethod(View v,int length)
	{
		boolean returnValue=false;
		if(length>25)
		{
			returnValue=true;
		}
		return returnValue;
	}
	public static boolean copyPasteMethod2(View v, int length) {
		boolean returnValue = false;
		if (length > 60) {
			returnValue = true;
		}
		return returnValue;
	}

	public static void setNotificationReceiver(Context context) {
		 alarmMgr0 = (AlarmManager)context. getSystemService(Context.ALARM_SERVICE);

		// Create pending intent & register it to your alarm notifier class
		Intent intent0 = new Intent(context, NoticeBoardReceiver.class);
	
		  pendingIntent0 = PendingIntent.getBroadcast(context, 0,
				intent0, PendingIntent.FLAG_UPDATE_CURRENT);
		
		// set that timer as a RTC Wakeup to alarm manager object
		alarmMgr0.setRepeating(AlarmManager.ELAPSED_REALTIME,
				SystemClock.elapsedRealtime(), (3*60*60*1000), pendingIntent0);
		
	}
	public static void cancelNotificationReceiver()
	{
		if(alarmMgr0!=null && pendingIntent0!=null)
		alarmMgr0.cancel(pendingIntent0);
	}
	
	public static Drawable getAssetImage(Context context, String filename) throws IOException {
		try {
		    AssetManager assets = context.getResources().getAssets();
		    InputStream buffer = new BufferedInputStream((assets.open("HariDrawable/" + filename + ".png")));
		    Bitmap bitmap = BitmapFactory.decodeStream(buffer);
		    return new BitmapDrawable(context.getResources(), bitmap);
		} catch (Exception e) {
			return null;
		}
	}
}
