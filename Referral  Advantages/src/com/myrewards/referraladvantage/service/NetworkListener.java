package com.myrewards.referraladvantage.service;

public interface NetworkListener {
	void onRequestCompleted(String response, String errorString, int eventType);

}
