package com.myrewards.referraladvantage.service;

public interface ServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
