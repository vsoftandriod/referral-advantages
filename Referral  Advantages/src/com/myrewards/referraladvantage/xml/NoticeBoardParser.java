package com.myrewards.referraladvantage.xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.myrewards.referraladvantage.model.NoticeBoard;

public class NoticeBoardParser {
	List<NoticeBoard> productsList;
	public void internalXMLParse(String response, List<NoticeBoard> productsList) {
		try {
			this.productsList = productsList;
		String _node,_element;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
		
			db = factory.newDocumentBuilder();
		
        InputSource inStream = new InputSource();
        inStream.setCharacterStream(new StringReader(response));
        Document doc = db.parse(inStream);  
        doc.getDocumentElement().normalize();
        
        NodeList list2 =    doc.getElementsByTagName("product");
        _node = new String();
        _element = new String();      
        
        for (int i=0;i<list2.getLength();i++){    
        	NodeList childNodes = list2.item(i).getChildNodes();
        	NoticeBoard product = new NoticeBoard();
             for(int j=0;j<childNodes.getLength();j++){
            	 Node value=childNodes.item(j).getChildNodes().item(0);
            	 _node=childNodes.item(j).getNodeName();
                 if(value != null){
                	 _element=value.getNodeValue(); 
                	 updateField(_node,_element, product);
                 }
             }
             productsList.add(product);
        }
        } catch (ParserConfigurationException e) {
			e.printStackTrace();
        } catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void updateField(String node, String element, NoticeBoard product) {
		if(node.equals("id"))
        {
			product.setId(element);
        }
		else if(node.equals("user_id"))
        {
			product.setUser_id(element);
        }
		else if(node.equals("client_id"))
        {
			product.setClient_id(element);
        }
		else if(node.equals("subject"))
        {
			product.setSubject(element);
        }
		else if(node.equals("details"))
        {
			product.setDetails(element);
        }
        else if(node.equals("created"))
        {
			product.setCreated(element);
        }
		else if(node.equals("starts"))
        {
			product.setStarts(element);
        }
		else if(node.equals("end"))
        {
			product.setEnd(element);
        }
		else if(node.equals("sort"))
        {
			product.setSort(element);
        }
		else if(node.equals("active"))
        {
			product.setActive(element);
        }
	}
	
}
